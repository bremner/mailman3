Þ    t     ¼  ó  \      (  ö  )  a   !     #     #     #     £#     ³#     Ä#     Ú#  T   ò#  Ã  G$  Ó   &    ß&     è(  ¿   )  ú   @*     ;+  }   K+  ç   É+  '   ±,  «   Ù,  ³   -  Y   9.  Ù   .  ^   m/  \   Ì/  $  )0  Y   N1  o   ¨1  _   2  G   x2     À2  q  Ñ2  â  C4  á  &6     8  ¯   8     D9     Ó9     ]:     ê:  y   ;  ß  ;  P   `>  I   ±>  Ú   û>  Û   Ö?  ã   ²@  :   A  P   ÑA  J   "B  9   mB  Ó   §B  Ð   {C  {   LD  b   ÈD  ^   +E  R   E  Ë   ÝE  í   ©F  g   G  >  ÿG  ¬   >I    ëI  q   L     ýL  I   M  ¦   ]M  ä   N    éN  n   ìO  ,  [P  ¸   Q  ë   AR  ÿ  -S  :   -U     hU  $   wU  !   U  K   ¾U  6   
V  =   AV  .   V  -   ®V  /   ÜV  <   W  Q   IW     W  M   ¹W  L   X     TX  0   jX  !   X  !   ½X  :   ßX  /   Y  7   JY  *   Y  4   ­Y     âY     ïY  3   Z  !   ?Z  K   aZ     ­Z     ÊZ  7   ÜZ      [      5[  3   V[  .   [  5   ¹[  %   ï[     \     )\     >\  <   T\  É   \  !   []     }]  Ñ  ]  B   h_     «_  -   Ã_  0   ñ_  (   "`  5   K`  2   `  8   ´`  )   í`  2   a  '   Ja  1   ra  #   ¤a  *   Èa     óa  "   b  @   6b      wb  	   b  #   ¢b  *   Æb     ñb  ,   c     /c     =c  &   Kc  4   rc     §c  3   Âc     öc  /   d     @d  $   Hd  "   md  x   d     	e     &e  ?   Ee     e  )    e  (   Êe     óe     f  (   -f  $   Vf  8   {f  1   ´f  !   æf    g  #    i  m  Di     ²j  "   Ñj  !   ôj  !   k  (   8k  9   ak  %   k  +   Ák  '   ík     l     1l  (   Il     rl  C   l     Ïl     Þl  #   öl  :   m  )   Um     m     m  d   ¸m  (   n  )   Fn  "   pn  %   n  8   ¹n  (   òn  "   o  '   >o     fo  ,   o      ²o  #   Óo     ÷o  *   p     9p     Jp  ,   bp     p  M   p  =   áp  O   q     oq     q     £q  ,   Ãq     ðq  #   r     *r     Ir  %   cr     r     r     ¶r  2   Ìr     ÿr  2   s  6   @s     ws  '   s     ³s  #   Äs  4   ès  =   t  1   [t  C   t  %   Ñt  )   ÷t  %   !u     Gu     Zu      xu  +   u  #  Åu  y  év     cx  0   px  !   ¡x  ,   Ãx     ðx     y     "y     :y  0   Xy     y  $   ¨y     Íy  '   çy     z  2   z     Nz     nz  6   z      ¾z  %   ßz  8   {     >{  .   V{      {  1   ¦{     Ø{  -   ò{      |     -|  -   B|     p|      |     ª|  ä   ¿|  "   ¤}  "   Ç}     ê}     	~     &~  w   E~  e   ½~    #     7  )   Â  6   ì      #  1   D  %   v  4     5   Ñ  6     7   >  6   v  "   ­  &   Ð  .   ÷     &  /   A  h   q  G  Ú  V   "      y          «     È     â  '   ÿ     '     9  	   P  ;   Z      2   ®  Ò   á  J   ´  D   ÿ  u  D  P   º  Q     >   ]  %     J   Â          ¥     ¼     Õ     ð     ù  !        $     ?     Q     d      |       /   ¦     Ö     ñ  !        3     Q     o  "     !   ²     Ô     ò          1     P     l           ¬     É  "   ã                ;     W     u          ¯     ³  (   Ê  Ö  ó  ö  Ê    Á     Õ     æ     û          "     3     I  T   a  Ã  ¶  Ó   z    N     W    ï  ú   ÿ     ú  }      ç      '   v¡  «   ¡  ³   J¢  Y   þ¢  Ù   X£  ^   2¤  \   ¤  $  î¤  Y   ¦  Á   m¦  _   /§  G   §     ×§  q  ê§  â  \©  U  ?«     ­  ¯   !®     Ñ®     `¯     ê¯  $   w°  y   °  ß  ±  P   ö³  I   G´  Ú   ´  Û   lµ  #  H¶  :   l·  P   §·  J   ø·  9   C¸  Ó   }¸    Q¹  {   pº  b   ìº  ^   O»  R   ®»  Ë   ¼  í   Í¼  g   »½  >  #¾  ¬   b¿  Æ  À  q   ÖÃ     HÄ  I   _Ä  ¦   ©Ä  ä   PÅ    5Æ  n   8Ç  ,  §Ç  ¸   ÔÈ  ë   É  ÿ  yÊ  :   yÌ     ´Ì  $   ÉÌ  ,   îÌ  f   Í  ;   Í  P   ¾Í  H   Î  0   XÎ  C   Î  M   ÍÎ  Q   Ï  -   mÏ  M   Ï  L   éÏ      6Ð  <   WÐ  2   Ð  $   ÇÐ  @   ìÐ  <   -Ñ  M   jÑ  5   ¸Ñ  >   îÑ     -Ò  '   ?Ò  3   gÒ  !   Ò  K   ½Ò     	Ó     %Ó  ]   :Ó  9   Ó  +   ÒÓ  J   þÓ  @   IÔ  5   Ô  -   ÀÔ  $   îÔ  +   Õ  (   ?Õ  <   hÕ  É   ¥Õ  9   oÖ     ©Ö  Ñ  ÆÖ  `   Ø      ùØ  2   Ù  ;   MÙ  >   Ù  5   ÈÙ  2   þÙ  H   1Ú  /   zÚ  _   ªÚ  7   
Û  I   BÛ  7   Û  =   ÄÛ     Ü  :   "Ü  k   ]Ü  )   ÉÜ     óÜ  )   Ý  *   -Ý     XÝ  B   iÝ  "   ¬Ý  "   ÏÝ  2   òÝ  S   %Þ     yÞ  K   Þ  ,   àÞ  ;   ß     Iß  /   Vß  T   ß  °   Ûß  '   à     ´à  ?   Óà     á  )   .á  (   Xá     á  *   á  @   Æá  $   â  D   ,â  i   qâ  4   Ûâ    ã  0   (å    Yå     tç  =   ç  !   Ñç  !   óç  (   è  9   >è  %   xè  C   è  '   âè     
é  ,   &é  >   Sé  /   é  r   Âé     5ê     Dê  #   \ê  :   ê  @   »ê  !   üê  1   ë     Pë  A   áë  K   #ì  R   oì  7   Âì  8   úì  (   3í  '   \í  '   í  9   ¬í  ?   æí  #   &î  0   Jî     {î  >   î     ×î  %   íî  D   ï     Xï  U   fï  X   ¼ï  j   ð     ð  %   ð     ¾ð  ?   Þð     ñ  *   9ñ  )   dñ     ñ  9   ¨ñ     âñ     ÷ñ     ò  2   %ò     Xò  2   fò  6   ò     Ðò  ;   äò      ó  #   8ó  {   \ó  h   Øó  L   Aô  C   ô  9   Òô  =   õ  %   Jõ     põ     õ      ¡õ  B   Âõ    ö  y  ÷     ù  0   ù  !   Dù  ;   fù  !   ¢ù     Äù     äù     üù  C   ú     ^ú  $   }ú  +   ¢ú  ,   Îú     ûú  2   	û     <û     \û  6   uû      ¬û  %   Íû  8   óû     ,ü  .   Dü      sü  1   ü  #   Æü  -   êü     ý     &ý  F   :ý  $   ý  /   ¦ý  "   Öý    ùý  @   ýþ  3   >ÿ  '   rÿ  8   ÿ  '   Óÿ  ¨   ûÿ     ¤    % ¼   ½ -   z Z   ¨ +    B   / 2   r 9   ¥ >   ß <    A   [ G    -   å 0    .   D %   s =    h   × G  @ V    )   ß    		 '   	    G	 $   a	 4   	    »	    Í	 	   ì	 R   ö	 ^  I
 6   ¨ Ò   ß O   ² J    (  M d   v Y   Û M   5 9    N   ½ Ã       Ð    ç             $ -   -     [    |        ² ì  Ñ    ¾ /   _ m      ý     Å   / j   õ K   ` =  ¬ M   ê    8 õ  : Ý   0     ! Ì  ! /  Ý# +  '    9* z  ;*   ¶+ z   ¿- Â  :. F  ý0 Á   D2 Ï  3 å  Ö4    ¼7    Ê7 b   á7       ù      Í   t   [  [       T  (   x   Â   G  z       0   ÿ   ®   5   §       ­   P      G   K  b  ²          »          *      4   t  o  ü   3   a       %  h  V     +             Q  
  {       ç         ê   ~   ú          Þ          Ì         «   a  ò   D   ;            Ö   K      H     h           æ   M  _          Ü   Ñ   |   g  q  <      6    À   4      O     A   Y         ?  Ú   ©   =   C  Ò       Æ       m  Ó   r  X           ô      È         n  Ï       9   2  $  !   Ç   ;  s  Ô   î   ]  @  ñ                       è   d  "           l   M   ë       ð               B      >  -  Z  Q     å          c            ³   #   é           W              £       à       7     U        ¿   e  õ   ]   k   ¡                       I   $   Ê   ã              °       ´       Ä   ¢   2           c    Ø       6   Y       ,  â   ¬   R   w       Õ   ×   ì          )     u      i  g   9     
      n       ·   ó        8   %   *       O  Á          '  ä   1   3  +   i   Z      D    ,         Ý   _   ¤          p   d     ö       µ   >   "       É      j   !     E      U     1  Î             =  ¯   0       C   ï   r       ±      k          :      p  .   ¨   L          s              N  b   ^  Û   ¥       7       Ù       `  8  ¼       B                          y                v   ¹   ½   J      ¶       /   l  L  û   º   e      ¦           -          m   F                ø      <           :     T         o           J  ?   Ð   S            H   )   Ë               Å   V           &      ÷   `           	       }           .  &               E          F   ^       (  @   q   í           R      A     S              Ã       j                      X  '   ß       5  N           ý   W      I      ¸      ¾               f      #  /  ª   þ              \  	  \      á   f   P       
    Run a script.  The argument is the module path to a callable.  This
    callable will be imported and then, if --listspec/-l is also given, is
    called with the mailing list as the first argument.  If additional
    arguments are given at the end of the command line, they are passed as
    subsequent positional arguments to the callable.  For additional help, see
    --details.

    If no --listspec/-l argument is given, the script function being called is
    called with no arguments.
     
    Start the named runner, which must be one of the strings returned by the -l
    option.

    For runners that manage a queue directory, optional `slice:range` if given
    is used to assign multiple runner processes to that queue.  range is the
    total number of runners for the queue while slice is the number of this
    runner from [0..range).  For runners that do not manage a queue, slice and
    range are ignored.

    When using the `slice:range` form, you must ensure that each runner for the
    queue is given the same range value.  If `slice:runner` is not given, then
    1:1 is used.
     
- Done. 
- Ignored: 
- Results: 
- Unprocessed: 
Held Messages:
 
Held Subscriptions:
 
Held Unsubscriptions:
     A more verbose output including the file system paths that Mailman is
    using.     A specification of the mailing list to operate on.  This may be the posting
    address of the list, or its List-ID.  The argument can also be a Python
    regular expression, in which case it is matched against both the posting
    address and List-ID of all mailing lists.  To use a regular expression,
    LISTSPEC must start with a ^ (and the matching is done with re.match().
    LISTSPEC cannot be a regular expression unless --run is given.     Add all member addresses in FILENAME with delivery mode as specified
    with -d/--delivery.  FILENAME can be '-' to indicate standard input.
    Blank lines and lines that start with a '#' are ignored.
         Add and delete members as necessary to syncronize a list's membership
    with an input file.  FILENAME is the file containing the new membership,
    one member per line.  Blank lines and lines that start with a '#' are
    ignored.  Addresses in FILENAME which are not current list members
    will be added to the list with delivery mode as specified with
    -d/--delivery.  List members whose addresses are not in FILENAME will
    be removed from the list.  FILENAME can be '-' to indicate standard input.
         Additional metadata key/value pairs to add to the message metadata
    dictionary.  Use the format key=value.  Multiple -m options are
    allowed.     Configuration file to use.  If not given, the environment variable
    MAILMAN_CONFIG_FILE is consulted and used if set.  If neither are given, a
    default configuration file is loaded.     Create a mailing list.

    The 'fully qualified list name', i.e. the posting address of the mailing
    list is required.  It must be a valid email address and the domain must be
    registered with Mailman.  List names are forced to lower case.     Date: $date     Delete all the members of the list.  If specified, none of -f/--file,
    -m/--member or --fromall may be specified.
         Delete list members whose addresses are in FILENAME in addition to those
    specified with -m/--member if any.  FILENAME can be '-' to indicate
    standard input.  Blank lines and lines that start with a '#' are ignored.
         Delete members from a mailing list.     Delete the list member whose address is ADDRESS in addition to those
    specified with -f/--file if any.  This option may be repeated for
    multiple addresses.
         Delete the member(s) specified by -m/--member and/or -f/--file from all
    lists in the installation.  This may not be specified together with
    -a/--all or -l/--list.
         Discard all shunted messages instead of moving them back to their original
    queue.     Display, add or delete a mailing list's members.
    Filtering along various criteria can be done when displaying.
    With no options given, displaying mailing list members
    to stdout is the default mode.
         Don't actually do anything, but in conjunction with --verbose, show what
    would happen.     Don't actually make the changes.  Instead, print out what would be
    done to the list.     Don't attempt to pretty print the object.  This is useful if there is some
    problem with the object and you just want to get an unpickled
    representation.  Useful with 'mailman qfile -i <file>'.  In that case, the
    list of unpickled objects will be left in a variable called 'm'.     Don't print status messages.  Error messages are still printed to standard
    error.     Don't restart the runners when they exit because of an error or a SIGUSR1.
    Use this only for debugging.     File to send the output to.  If not given, or if '-' is given, standard
    output is used.     File to send the output to.  If not given, standard output is used.     From: $from_     Generate the MTA alias files upon startup. Some MTA, like postfix, can't
    deliver email if alias files mentioned in its configuration are not
    present. In some situations, this could lead to a deadlock at the first
    start of mailman3 server. Setting this option to true will make this
    script create the files and thus allow the MTA to operate smoothly.     If the master watcher finds an existing master lock, it will normally exit
    with an error message.  With this option, the master will perform an extra
    level of checking.  If a process matching the host/pid described in the
    lock file is running, the master will still exit, requiring you to manually
    clean up the lock.  But if no matching process is found, the master will
    remove the apparently stale lock and make another attempt to claim the
    master lock.     If the master watcher finds an existing master lock, it will normally exit
    with an error message.  With this option,the master will perform an extra
    level of checking.  If a process matching the host/pid described in the
    lock file is running, the master will still exit, requiring you to manually
    clean up the lock.  But if no matching process is found, the master will
    remove the apparently stale lock and make another attempt to claim the
    master lock.     Import Mailman 2.1 list data.  Requires the fully-qualified name of the
    list to import and the path to the Mailman 2.1 pickle file.     Increment the digest volume number and reset the digest number to one.  If
    given with --send, the volume number is incremented before any current
    digests are sent.     Key to use for the lookup.  If no section is given, all the key-values pair
    from any section matching the given key will be displayed.     Leaves you at an interactive prompt after all other processing is complete.
    This is the default unless the --run option is given.     List only those mailing lists hosted on the given domain, which
    must be the email host name.  Multiple -d options may be given.
         Message-ID: $message_id     Name of file containing the message to inject.  If not given, or
    '-' (without the quotes) standard input is used.     Normally, this script will refuse to run if the user id and group id are
    not set to the 'mailman' user and group (as defined when you configured
    Mailman).  If run as root, this script will change to this user and group
    before the check is made.

    This can be inconvenient for testing and debugging purposes, so the -u flag
    means that the step that sets and checks the uid/gid is skipped, and the
    program is run as the current user and group.  This flag is not recommended
    for normal production environments.

    Note though, that if you run with -u and are not in the mailman group, you
    may have permission problems, such as being unable to delete a list's
    archives through the web.  Tough luck!     Notify the list owner by email that their mailing list has been
    created.     Operate on a mailing list.

    For detailed help, see --details
         Operate on this mailing list.  Multiple --list options can be given.  The
    argument can either be a List-ID or a fully qualified list name.  Without
    this option, operate on the digests for all mailing lists.     Operate on this mailing list.  Multiple --list options can be given.  The
    argument can either be a List-ID or a fully qualified list name.  Without
    this option, operate on the requests for all mailing lists.     Override the default set of runners that the master will invoke, which is
    typically defined in the configuration file.  Multiple -r options may be
    given.  The values for -r are passed straight through to bin/runner.     Override the list's setting for admin_notify_mchanges.     Override the list's setting for send_goodbye_message to
    deleted members.     Override the list's setting for send_welcome_message to added members.     Override the list's setting for send_welcome_message.     Register the mailing list's domain if not yet registered.  This is
    the default behavior, but these options are provided for backward
    compatibility.  With -D do not register the mailing list's domain.     Run the named runner exactly once through its main loop.  Otherwise, the
    runner runs indefinitely until the process receives a signal.  This is not
    compatible with runners that cannot be run once.     Section to use for the lookup.  If no key is given, all the key-value pairs
    of the given section will be displayed.     Send any collected digests for the List only if their digest_send_periodic
    is set to True.     Send any collected digests right now, even if the size threshold has not
    yet been met.     Send the added members an invitation rather than immediately adding them.
         Set the added members delivery mode to 'regular', 'mime', 'plain',
    'summary' or 'disabled'.  I.e., one of regular, three modes of digest
    or no delivery.  If not given, the default is regular.     Set the added members delivery mode to 'regular', 'mime', 'plain',
    'summary' or 'disabled'.  I.e., one of regular, three modes of digest
    or no delivery.  If not given, the default is regular.  Ignored for invited
    members.     Set the list's preferred language to CODE, which must be a registered two
    letter language code.     Specify a list owner email address.  If the address is not currently
    registered with Mailman, the address is registered and linked to a user.
    Mailman will send a confirmation message to the address, but it will also
    send a list creation notice to the address.  More than one owner can be
    specified.     Specify the encoding of strings in PICKLE_FILE if not utf-8 or a subset
    thereof. This will normally be the Mailman 2.1 charset of the list's
    preferred_language.     Start a runner.

    The runner named on the command line is started, and it can either run
    through its main loop once (for those runners that support this) or
    continuously.  The latter is how the master runner starts all its
    subprocesses.

    -r is required unless -l or -h is given, and its argument must be one of
    the names displayed by the -l switch.

    Normally, this script should be started from `mailman start`.  Running it
    separately or with -o is generally useful only for debugging.  When run
    this way, the environment variable $MAILMAN_UNDER_MASTER_CONTROL will be
    set which subtly changes some error handling behavior.
         Start an interactive Python session, with a variable called 'm'
    containing the list of unpickled objects.     Subject: $subject     The list to operate on.  Required unless --fromall is specified.
         The name of the queue to inject the message to.  QUEUE must be one of the
    directories inside the queue directory.  If omitted, the incoming queue is
    used.     [MODE] Add all member addresses in FILENAME.  FILENAME can be '-' to
    indicate standard input.  Blank lines and lines that start with a
    '#' are ignored.  This option is deprecated in favor of 'mailman
    addmembers'.     [MODE] Delete all member addresses found in FILENAME
    from the specified list. FILENAME can be '-' to indicate standard input.
    Blank lines and lines that start with a '#' are ignored.
    This option is deprecated in favor of 'mailman delmembers'.     [MODE] Display output to FILENAME instead of stdout.  FILENAME
    can be '-' to indicate standard output.     [MODE] Synchronize all member addresses of the specified mailing list
    with the member addresses found in FILENAME.
    FILENAME can be '-' to indicate standard input.
    Blank lines and lines that start with a '#' are ignored.
    This option is deprecated in favor of 'mailman syncmembers'.     [output filter] Display only digest members of kind.
    'any' means any digest type, 'plaintext' means only plain text (rfc 1153)
    type digests, 'mime' means MIME type digests.     [output filter] Display only members with a given ROLE.
    The role may be 'any', 'member', 'nonmember', 'owner', 'moderator',
    or 'administrator' (i.e. owners and moderators).
    If not given, then delivery members are used.      [output filter] Display only members with a given delivery status.
    'enabled' means all members whose delivery is enabled, 'any' means
    members whose delivery is disabled for any reason, 'byuser' means
    that the member disabled their own delivery, 'bybounces' means that
    delivery was disabled by the automated bounce processor,
    'byadmin' means delivery was disabled by the list
    administrator or moderator, and 'unknown' means that delivery was disabled
    for unknown (legacy) reasons.     [output filter] Display only regular delivery members.  (Digest mode) $count matching mailing lists found: $display_name post acknowledgment $member unsubscribed from ${mlist.display_name} mailing list due to bounces $member's subscription disabled on $mlist.display_name $mlist.display_name Digest, Vol $volume, Issue $digest_number $mlist.display_name mailing list probe message $mlist.display_name subscription notification $mlist.display_name unsubscription notification $mlist.fqdn_listname post from $msg.sender requires approval $mlist.list_id bumped to volume $mlist.volume, number ${mlist.next_digest_number} $mlist.list_id has no members $mlist.list_id is at volume $mlist.volume, number ${mlist.next_digest_number} $mlist.list_id sent volume $mlist.volume, number ${mlist.next_digest_number} $name runs $classname $person has a pending subscription for $listname $person left $mlist.fqdn_listname $realname via $mlist.display_name $self.name: $email is not a member of $mlist.fqdn_listname $self.name: No valid address found to subscribe $self.name: No valid email address found to unsubscribe $self.name: no such command: $command_name $self.name: too many arguments: $printable_arguments (no subject) - Original message details: --send and --periodic flags cannot be used together <----- start object $count -----> A previous run of GNU Mailman did not exit cleanly ({}).  Try using --force A rule which always matches. Accept a message. Add a list-specific prefix to the Subject header value. Add the RFC 2369 List-* headers. Add the message to the archives. Add the message to the digest, possibly sending it. After content filtering, the message was empty Already subscribed (skipping): $display_name <$email> Already subscribed (skipping): $email An alias for 'end'. An alias for 'join'. An alias for 'leave'. An alternative directory to output the various MTA files to. And you can print the list's request address by running:

% mailman withlist -r listaddr.requestaddr -l ant@example.com
Importing listaddr ...
Running listaddr.requestaddr() ...
ant-request@example.com Announce only mailing list style. Apply DMARC mitigations. As another example, say you wanted to change the display name for a particular
mailing list.  You could put the following function in a file called
`change.py`:

def change(mlist, display_name):
    mlist.display_name = display_name

and run this from the command line:

% mailman withlist -r change -l ant@example.com 'My List'

Note that you do not have to explicitly commit any database transactions, as
Mailman will do this for you (assuming no errors occured). Auto-response for your message to the "$display_name" mailing list Bad runner spec: $value Calculate the owner and moderator recipients. Calculate the regular recipients of the message. Cannot import runner module: $class_path Cannot parse as valid email address (skipping): $line Cannot unshunt message $filebase, skipping:
$error Catch messages that are bigger than a specified maximum. Catch messages with implicit destination. Catch messages with no, or empty, Subject headers. Catch messages with suspicious headers. Catch messages with too many explicit recipients. Catch mis-addressed email commands. Cleanse certain headers from all messages. Confirm a subscription request. Confirmation email sent to $person Confirmation email sent to $person to leave $mlist.fqdn_listname Confirmation token did not match Confirmed Content filter message notification Created mailing list: $mlist.fqdn_listname DMARC moderation Decorate a message with headers and footers. Digest Footer Digest Header Discard a message and stop processing. Discussion mailing list style with private archives. Display Mailman's version. Display more debugging information to the log file. Echo back your arguments. Emergency moderation is in effect for this list End of  Filter the MIME content of messages. Find DMARC policy of From: domain. For unknown reasons, the master lock could not be acquired.

Lock file: $config.LOCK_FILE
Lock host: $hostname

Exiting. Forward of moderated message GNU Mailman is already running GNU Mailman is in an unexpected state ($hostname != $fqdn_name) GNU Mailman is not running GNU Mailman is running (master pid: $pid) GNU Mailman is stopped (stale pid: $pid) Generating MTA alias maps Get a list of the list members. Get help about available email commands. Get information out of a queue file. Get the normal delivery recipients from an include file. Header "{}" matched a bounce_matching_header line Header "{}" matched a header rule Here's an example of how to use the --run option.  Say you have a file in the
Mailman installation directory called 'listaddr.py', with the following two
functions:

def listaddr(mlist):
    print(mlist.posting_address)

def requestaddr(mlist):
    print(mlist.request_address)

Run methods take at least one argument, the mailing list object to operate
on.  Any additional arguments given on the command line are passed as
positional arguments to the callable.

If -l is not given then you can run a function that takes no arguments.
 Hold a message and stop processing. If you reply to this message, keeping the Subject: header intact, Mailman will
discard the held message.  Do this if the message is spam.  If you reply to
this message and include an Approved: header with the list password in it, the
message will be approved for posting to the list.  The Approved: header can
also appear in the first line of the body of the reply. Ignoring non-dictionary: {0!r} Ignoring non-text/plain MIME parts Illegal list name: $fqdn_listname Illegal owner addresses: $invalid Information about this Mailman instance. Inject a message from a file into a mailing list's queue. Invalid language code: $language_code Invalid or unverified email address: $email Invalid value for [shell]use_python: {} Is the master even running? Join this mailing list. Last autoresponse notification for today Leave this mailing list. Leave this mailing list.

You may be asked to confirm your request. Less verbosity List all mailing lists. List already exists: $fqdn_listname List only those mailing lists that are publicly advertised List the available runner names and exit. Look for a posting loop. Look for any previous rule hit. Match all messages posted to a mailing list that gateways to a
        moderated newsgroup.
         Match messages sent by banned addresses. Match messages sent by moderated members. Match messages sent by nonmembers. Match messages with no valid senders. Member not subscribed (skipping): $display_name <$email> Member not subscribed (skipping): $email Members of the {} mailing list:
{} Membership is banned (skipping): $email Message contains administrivia Message has already been posted to this list Message has implicit destination Message has more than {} recipients Message has no subject Message sender {} is banned from this list Moderation chain Modify message headers. Move the message to the outgoing news queue. N/A New subscription request to $self.mlist.display_name from $self.address.email New unsubscription request from $mlist.display_name by $email New unsubscription request to $self.mlist.display_name from $self.address.email No child with pid: $pid No confirmation token found No matching mailing lists found No registered user for email address: $email No runner name given. No sender was found in the message. No such command: $command_name No such list found: $spec No such list matching spec: $listspec No such list: $_list No such list: $listspec No such queue: $queue Not a Mailman 2.1 configuration file: $pickle_file Nothing to do Notify list owners/moderators of pending requests. Number of objects found (see the variable 'm'): $count Operate on digests. Ordinary discussion mailing list style. Original Message PID unreadable in: $config.PID_FILE Perform ARC auth checks and attach resulting headers Perform auth checks and attach Authentication-Results header. Perform some bookkeeping after a successful post. Poll the NNTP server for messages to be gatewayed to mailing lists. Post to a moderated newsgroup gateway Posting of your message titled "$subject" Print detailed instructions and exit. Print less output. Print some additional status. Print the Mailman configuration. Process DMARC reject or discard mitigations Produces a list of member names and email addresses.

The optional delivery= and mode= arguments can be used to limit the report
to those members with matching delivery status and/or delivery mode.  If
either delivery= or mode= is specified more than once, only the last occurrence
is used.
 Programmatically, you can write a function to operate on a mailing list, and
this script will take care of the housekeeping (see below for examples).  In
that case, the general usage syntax is:

% mailman withlist [options] -l listspec [args ...]

where `listspec` is either the posting address of the mailing list
(e.g. ant@example.com), or the List-ID (e.g. ant.example.com). Reason: {}

 Regenerate the aliases appropriate for your MTA. Regular expression requires --run Reject/bounce a message and stop processing. Remove DomainKeys headers. Remove a mailing list. Removed list: $listspec Reopening the Mailman runners Request to mailing list "$display_name" rejected Restarting the Mailman runners Send an acknowledgment of a posting. Send automatic responses. Send the message to the outgoing queue. Sender: {}
 Show a list of all available queue names and exit. Show also the list descriptions Show also the list names Show the current running status of the Mailman system. Show this help message and exit. Shutting down Mailman's master runner Signal the Mailman processes to re-open their log files. Stale pid file removed. Start the Mailman master and runner processes. Starting Mailman's master runner Stop and restart the Mailman runner subprocesses. Stop processing commands. Stop the Mailman master and runner processes. Subject: {}
 Subscription request Suppress some duplicates of the same message. Suppress status messages Tag messages with topic matches. The Mailman Replybot The attached message matched the $mlist.display_name mailing list's content
filtering rules and was prevented from being forwarded on to the list
membership.  You are receiving the only remaining copy of the discarded
message.

 The built-in -owner posting chain. The built-in header matching chain The built-in moderation chain. The built-in owner pipeline. The built-in posting pipeline. The mailing list is in emergency hold and this message was not
        pre-approved by the list administrator.
         The master lock could not be acquired because it appears as though another
master is already running. The master lock could not be acquired, because it appears as if some process
on some other host may have acquired it.  We can't test for stale locks across
host boundaries, so you'll have to clean this up manually.

Lock file: $config.LOCK_FILE
Lock host: $hostname

Exiting. The master lock could not be acquired.  It appears as though there is a stale
master lock.  Try re-running $program with the --force flag. The message comes from a moderated member The message has a matching Approve or Approved header. The message has no valid senders The message is larger than the {} KB maximum size The message is not from a list member The message's content type was explicitly disallowed The message's content type was not explicitly allowed The message's file extension was explicitly disallowed The message's file extension was not explicitly allowed The results of your email command are provided below.
 The results of your email commands The sender is in the nonmember {} list The variable 'm' is the $listspec mailing list The virgin queue pipeline. The {} list has {} moderation requests waiting. The {} list has {} moderation requests waiting.

{}
Please attend to this at your earliest convenience.
 There are two ways to use this script: interactively or programmatically.
Using it interactively allows you to play with, examine and modify a mailing
list from Python's interactive interpreter.  When running interactively, the
variable 'm' will be available in the global namespace.  It will reference the
mailing list object. This script provides you with a general framework for interacting with a
mailing list. Today's Topics ($count messages) Today's Topics:
 Uncaught bounce notification Undefined domain: $domain Undefined runner name: $name Unrecognized or invalid argument(s):
{} Unshunt messages. Unsubscription request User: {}
 Welcome to the "$mlist.display_name" mailing list${digmode} You are not allowed to post to this mailing list From: a domain which publishes a DMARC policy of reject or quarantine, and your message has been automatically rejected.  If you think that your messages are being rejected in error, contact the mailing list owner at ${listowner}. You are not authorized to see the membership list. You can print the list's posting address by running the following from the
command line:

% mailman withlist -r listaddr -l ant@example.com
Importing listaddr ...
Running listaddr.listaddr() ...
ant@example.com You have been invited to join the $event.mlist.fqdn_listname mailing list. You have been unsubscribed from the $mlist.display_name mailing list You will be asked to confirm your subscription request and you may be issued a
provisional password.

By using the 'digest' option, you can specify whether you want digest delivery
or not.  If not specified, the mailing list's default delivery mode will be
used.  You can use the 'address' option to request subscription of an address
other than the sender of the command.
 Your confirmation is needed to join the $event.mlist.fqdn_listname mailing list. Your confirmation is needed to leave the $event.mlist.fqdn_listname mailing list. Your message to $mlist.fqdn_listname awaits moderator approval Your new mailing list: $fqdn_listname Your subscription for ${mlist.display_name} mailing list has been disabled Your urgent message to the $mlist.display_name mailing list was not authorized
for delivery.  The original message as received by Mailman is attached.
 [$mlist.display_name]  [----- end pickle -----] [----- start pickle -----] [ADD] %s [DEL] %s [No bounce details are available] [No details are available] [No reason given] [No reasons given] bad argument: $argument domain:admin:notice:new-list.txt help.txt ipython is not available, set use_ipython to no list:admin:action:post.txt list:admin:action:subscribe.txt list:admin:action:unsubscribe.txt list:admin:notice:disable.txt list:admin:notice:removal.txt list:admin:notice:subscribe.txt list:admin:notice:unrecognized.txt list:admin:notice:unsubscribe.txt list:member:digest:header.txt list:member:digest:masthead.txt list:member:generic:footer.txt list:member:regular:header.txt list:user:action:invite.txt list:user:action:subscribe.txt list:user:action:unsubscribe.txt list:user:notice:goodbye.txt list:user:notice:hold.txt list:user:notice:no-more-today.txt list:user:notice:post.txt list:user:notice:probe.txt list:user:notice:refuse.txt list:user:notice:rejected.txt list:user:notice:warning.txt list:user:notice:welcome.txt n/a readline not available slice and range must be integers: $value Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-12-11 04:16+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>
Language-Team: Hebrew <https://hosted.weblate.org/projects/gnu-mailman/mailman/he/>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && n % 10 == 0) ? 2 : 3));
X-Generator: Weblate 4.4-dev
 
    Run a script.  The argument is the module path to a callable.  This
    callable will be imported and then, if --listspec/-l is also given, is
    called with the mailing list as the first argument.  If additional
    arguments are given at the end of the command line, they are passed as
    subsequent positional arguments to the callable.  For additional help, see
    --details.

    If no --listspec/-l argument is given, the script function being called is
    called with no arguments.
     
    ××¤×¢××ª ×××¦× ×©×¦××× ×©×××× ×××××ª ×××ª ×××××¨××××ª ×©×××××¨× ×¢× ××× ×××¤×©×¨××ª â-l.

    ×××¦× ×× ×©×× ×××× ×ª××§×××ª ×ª××¨××, ××© `×××ª××:××××` ×× ×× ×©×¡××¤×§ ××©××© ×××§×¦××ª
    ××¡×¤×¨ ×ª××××× ××¦× ×× ××ª××¨ ×××.  ××××× ××× ××¡×¤×¨ ×××¦× ×× ××××× ××ª××¨ ××¢×× ×××ª××
    ××× ××¡×¤×¨ ×××¦× ××× ×Ö¾0 ××¢× ××××. ×××¦× ×× ×©×× ×× ×××× ×ª××¨, ××× ××ª××××¡××ª
    ××××ª×× ×× ×××××.

    ××¢×ª ×©××××© ××¦××¨× `×××ª××:××××` ×¢××× ××××× ×©×× ××¦× ×¢×××¨ ××ª××¨ ××§×× ××ª ×××ª×
    ×¢×¨× ××××.  ×× `×××ª××:××××` ×× ×¡××¤×§, ×× × ×¢×©× ×©××××© ×Ö¾1:1.
     
- ××¡×ª×××. 
- ×××ª×¢××××ª: 
- ×ª××¦×××ª: 
- ××× ×¢××××: 
Held Messages:
 
Held Subscriptions:
 
Held Unsubscriptions:
     A more verbose output including the file system paths that Mailman is
    using.     A specification of the mailing list to operate on.  This may be the posting
    address of the list, or its List-ID.  The argument can also be a Python
    regular expression, in which case it is matched against both the posting
    address and List-ID of all mailing lists.  To use a regular expression,
    LISTSPEC must start with a ^ (and the matching is done with re.match().
    LISTSPEC cannot be a regular expression unless --run is given.     Add all member addresses in FILENAME with delivery mode as specified
    with -d/--delivery.  FILENAME can be '-' to indicate standard input.
    Blank lines and lines that start with a '#' are ignored.
         Add and delete members as necessary to syncronize a list's membership
    with an input file.  FILENAME is the file containing the new membership,
    one member per line.  Blank lines and lines that start with a '#' are
    ignored.  Addresses in FILENAME which are not current list members
    will be added to the list with delivery mode as specified with
    -d/--delivery.  List members whose addresses are not in FILENAME will
    be removed from the list.  FILENAME can be '-' to indicate standard input.
         Additional metadata key/value pairs to add to the message metadata
    dictionary.  Use the format key=value.  Multiple -m options are
    allowed.     ×§×××¥ ××××¨××ª ××©××××©.  ×× ×× ×¦×××, ×××××¨××ª ×©×××¤××¢××ª ×××©×ª× × ××¡××××ª×
    MAILMAN_CONFIG_FILE ×ª×××× × ××¤×¢××××ª.  ×× ×× ×¦××× × ××£ ×××ª ×××,
    ×××¢× ×§×××¥ ××××¨××ª ×××¨×¨×ª ××××.     Create a mailing list.

    The 'fully qualified list name', i.e. the posting address of the mailing
    list is required.  It must be a valid email address and the domain must be
    registered with Mailman.  List names are forced to lower case.     ×ª××¨××: $date     Delete all the members of the list.  If specified, none of -f/--file,
    -m/--member or --fromall may be specified.
         Delete list members whose addresses are in FILENAME in addition to those
    specified with -m/--member if any.  FILENAME can be '-' to indicate
    standard input.  Blank lines and lines that start with a '#' are ignored.
         Delete members from a mailing list.     Delete the list member whose address is ADDRESS in addition to those
    specified with -f/--file if any.  This option may be repeated for
    multiple addresses.
         Delete the member(s) specified by -m/--member and/or -f/--file from all
    lists in the installation.  This may not be specified together with
    -a/--all or -l/--list.
         Discard all shunted messages instead of moving them back to their original
    queue.     Display, add or delete a mailing list's members.
    Filtering along various criteria can be done when displaying.
    With no options given, displaying mailing list members
    to stdout is the default mode.
         Don't actually do anything, but in conjunction with --verbose, show what
    would happen.     Don't actually make the changes.  Instead, print out what would be
    done to the list.     Don't attempt to pretty print the object.  This is useful if there is some
    problem with the object and you just want to get an unpickled
    representation.  Useful with 'mailman qfile -i <file>'.  In that case, the
    list of unpickled objects will be left in a variable called 'm'.     Don't print status messages.  Error messages are still printed to standard
    error.     ×× ×××¤×¢×× ××ª ×××¦× ×× ××××© ××©×× ××¡×ª××××× ×¢×§× ×©×××× ×× SIGUSR1 (×××ª ××©×ª××© 1).
    ××© ×××©×ª××© ××× ×× ××¤×× ×©×××××ª ××××.     File to send the output to.  If not given, or if '-' is given, standard
    output is used.     File to send the output to.  If not given, standard output is used.     ×××ª: $from_     Generate the MTA alias files upon startup. Some MTA, like postfix, can't
    deliver email if alias files mentioned in its configuration are not
    present. In some situations, this could lead to a deadlock at the first
    start of mailman3 server. Setting this option to true will make this
    script create the files and thus allow the MTA to operate smoothly.     If the master watcher finds an existing master lock, it will normally exit
    with an error message.  With this option, the master will perform an extra
    level of checking.  If a process matching the host/pid described in the
    lock file is running, the master will still exit, requiring you to manually
    clean up the lock.  But if no matching process is found, the master will
    remove the apparently stale lock and make another attempt to claim the
    master lock.     ×× ×××©××× ××¨××©× ×××¦× × ×¢××× ×¨××©××ª ×§××××ª, ××× ×××¨× ××× ×××¦× ×¢× ××××¢×ª
    ×©××××. ×¢× ××¤×©×¨××ª ××, ×××©××× ×××××§ ××¨×× × ××¡×¤×ª. ×× ×ª×××× ×ª××× ××××¨×/
    ×××× ××©×ª××© ×©××ª×××¨ ××§×××¥ ×× ×¢××× × ××¦× ×¤×¢××, ×××©××× ×¢×××× ××¦×, ××
    ×©×××¨××© ××× ×× ×§××ª ××ª ×× ×¢××× ××× ××ª. ×× ×× × ××¦× ×ª×××× ×ª××× ×××©××× ××¡××¨
    ××ª ×× ×¢××× ×××××ª×¨×ª ××× ×¡× ×××©×× ××ª ×× ×¢××× ××¨××©××ª ×¤×¢× × ××¡×¤×ª.     Import Mailman 2.1 list data.  Requires the fully-qualified name of the
    list to import and the path to the Mailman 2.1 pickle file.     Increment the digest volume number and reset the digest number to one.  If
    given with --send, the volume number is incremented before any current
    digests are sent.     Key to use for the lookup.  If no section is given, all the key-values pair
    from any section matching the given key will be displayed.     Leaves you at an interactive prompt after all other processing is complete.
    This is the default unless the --run option is given.     List only those mailing lists hosted on the given domain, which
    must be the email host name.  Multiple -d options may be given.
         ×××× ××××¢×: $message_id     Name of file containing the message to inject.  If not given, or
    '-' (without the quotes) standard input is used.     Normally, this script will refuse to run if the user id and group id are
    not set to the 'mailman' user and group (as defined when you configured
    Mailman).  If run as root, this script will change to this user and group
    before the check is made.

    This can be inconvenient for testing and debugging purposes, so the -u flag
    means that the step that sets and checks the uid/gid is skipped, and the
    program is run as the current user and group.  This flag is not recommended
    for normal production environments.

    Note though, that if you run with -u and are not in the mailman group, you
    may have permission problems, such as being unable to delete a list's
    archives through the web.  Tough luck!     Notify the list owner by email that their mailing list has been
    created.     Operate on a mailing list.

    For detailed help, see --details
         Operate on this mailing list.  Multiple --list options can be given.  The
    argument can either be a List-ID or a fully qualified list name.  Without
    this option, operate on the digests for all mailing lists.     Operate on this mailing list.  Multiple --list options can be given.  The
    argument can either be a List-ID or a fully qualified list name.  Without
    this option, operate on the requests for all mailing lists.     ××¨××¡×ª ×¢×¨××ª ×××¦× ×× ×××¨×¨×ª ×××× ×©××¨××©× ××¤×¢××, ×©××× ×××¨× ××× ×××××¨×ª
    ××§×××¥ ×××××¨××ª.  × ××ª× ××¡×¤×§ ××××× ××¤×©×¨××××ª â-r.  ××¢×¨××× ×¢×××¨ â-r ×××¢××¨××
    ××©××¨××ª ×× ×××× ×¨×/××¦×.     Override the list's setting for admin_notify_mchanges.     Override the list's setting for send_goodbye_message to
    deleted members.     Override the list's setting for send_welcome_message to added members.     Override the list's setting for send_welcome_message.     Register the mailing list's domain if not yet registered.  This is
    the default behavior, but these options are provided for backward
    compatibility.  With -D do not register the mailing list's domain.     ×××¤×¢×× ××ª ×××¦× ×©×¦××× ×××××§ ×¤×¢× ×××ª ××¨× ×××××× ××¨××©××ª ×©××. ×××¨×ª, ×××¦×
    ××¤×¢× ××××¤× ×××××¨× ×¢× ×©××ª×××× ××§×× ×××ª ×¡×××. ×× ×ª××× ×××¦× ×× ×©×× ××××××
    ××¤×¢×× ×¤×¢× ×××ª.     Section to use for the lookup.  If no key is given, all the key-value pairs
    of the given section will be displayed.     Send any collected digests for the List only if their digest_send_periodic
    is set to True.     Send any collected digests right now, even if the size threshold has not
    yet been met.     Send the added members an invitation rather than immediately adding them.
         Set the added members delivery mode to 'regular', 'mime', 'plain',
    'summary' or 'disabled'.  I.e., one of regular, three modes of digest
    or no delivery.  If not given, the default is regular.     Set the added members delivery mode to 'regular', 'mime', 'plain',
    'summary' or 'disabled'.  I.e., one of regular, three modes of digest
    or no delivery.  If not given, the default is regular.  Ignored for invited
    members.     Set the list's preferred language to CODE, which must be a registered two
    letter language code.     Specify a list owner email address.  If the address is not currently
    registered with Mailman, the address is registered and linked to a user.
    Mailman will send a confirmation message to the address, but it will also
    send a list creation notice to the address.  More than one owner can be
    specified.     Specify the encoding of strings in PICKLE_FILE if not utf-8 or a subset
    thereof. This will normally be the Mailman 2.1 charset of the list's
    preferred_language.     ××¤×¢××ª ××¦×.

    ×××¦× ×©×¦××× ××©××¨×ª ××¤×§××× ×××¤×¢× ×××× ×××× ××¤×¢×× ××¨× ×××××× ××¨××©×× ×
    ×©×× ×¢× ×××ª (×××¦× ×× ×©×ª××××× ×××) ×× ××××¤× ×¨×¦××£. ×××¤×©×¨××ª ××©× ×××
    ××× ×××¨× ×× ×××¦× ××¨××©× ××¤×¢×× ××ª ×× ×ª×ªÖ¾××ª×××××× ×©××.

    â-r ××¨××© ××××× ×¡××¤×§× â-l ×× â-h, ××××©×ª× ×× ×©×× ×××××× ×××××ª ××× ×××©×××ª
    ×©××¦××× ×× ×¢× ××× ××¤×§××× â-l.

    ×××¨× ×××, ××ª ××¡×§×¨××¤× ××× ××© ×××¤×¢×× ××¨× `mailman start`.  ××¤×¢××ª×
    ×× ×¤×¨× ×× ×¢× -o ×©××××©××ª ×××¨× ××× ×× ××¤×× ×©×××××ª.  ××¤×¢×× ××××¤× ×××
    ×××××× ××× ×©×××©×ª× × ××¡××××ª× â$MAILMAN_UNDER_MASTER_CONTROL
    ×××××¨, ×× ×©××××× ××©×× ×××× ×§××× ××××§ ×××ª× ××××ª ××××¤×× ××©×××××ª.
         Start an interactive Python session, with a variable called 'm'
    containing the list of unpickled objects.     × ××©×: $subject     The list to operate on.  Required unless --fromall is specified.
         The name of the queue to inject the message to.  QUEUE must be one of the
    directories inside the queue directory.  If omitted, the incoming queue is
    used.     [MODE] Add all member addresses in FILENAME.  FILENAME can be '-' to
    indicate standard input.  Blank lines and lines that start with a
    '#' are ignored.  This option is deprecated in favor of 'mailman
    addmembers'.     [MODE] Delete all member addresses found in FILENAME
    from the specified list. FILENAME can be '-' to indicate standard input.
    Blank lines and lines that start with a '#' are ignored.
    This option is deprecated in favor of 'mailman delmembers'.     [MODE] Display output to FILENAME instead of stdout.  FILENAME
    can be '-' to indicate standard output.     [MODE] Synchronize all member addresses of the specified mailing list
    with the member addresses found in FILENAME.
    FILENAME can be '-' to indicate standard input.
    Blank lines and lines that start with a '#' are ignored.
    This option is deprecated in favor of 'mailman syncmembers'.     [output filter] Display only digest members of kind.
    'any' means any digest type, 'plaintext' means only plain text (rfc 1153)
    type digests, 'mime' means MIME type digests.     [output filter] Display only members with a given ROLE.
    The role may be 'any', 'member', 'nonmember', 'owner', 'moderator',
    or 'administrator' (i.e. owners and moderators).
    If not given, then delivery members are used.      [output filter] Display only members with a given delivery status.
    'enabled' means all members whose delivery is enabled, 'any' means
    members whose delivery is disabled for any reason, 'byuser' means
    that the member disabled their own delivery, 'bybounces' means that
    delivery was disabled by the automated bounce processor,
    'byadmin' means delivery was disabled by the list
    administrator or moderator, and 'unknown' means that delivery was disabled
    for unknown (legacy) reasons.     [output filter] Display only regular delivery members.  (××¦× ×ª×§×¦××¨) $count matching mailing lists found: ×××¨× ××¤×¨×¡×××× ×©× $display_name ×××× ×× ×©× $member ××¨×©×××ª ××××××¨ ${mlist.display_name} ×××× ×¢×§× ××××¨××ª ×××× ×× ×©× $member ×¢× $mlist.display_name ×××× ×ª×§×¦××¨ ×©× $mlist.display_name, ××¨× $volume, ×××××× $digest_number ××××¢×ª ×ª×©××× ××× ×¨×©×××ª ××××××¨ $mlist.display_name ××××¢×ª ××× ×× ××× $mlist.display_name ××××¢×ª ××××× ××× ×× ×××¨×©××× $mlist.display_name ××¤×¨×¡×× ×©× $mlist.fqdn_listname ×××ª $msg.sender ×××¨×© ×××©××¨ $mlist.list_id bumped to volume $mlist.volume, number ${mlist.next_digest_number} ××¨×©××× $mlist.list_id ××× ×××¨×× $mlist.list_id is at volume $mlist.volume, number ${mlist.next_digest_number} $mlist.list_id sent volume $mlist.volume, number ${mlist.next_digest_number} $name ××¤×¢×× ××ª $classname ××§×©×ª ××¨×©×× ×©× $person ×× $listname ×××ª×× × $person ××××¥ ××¨×©××× $mlist.fqdn_listname $realname ××¨× $mlist.display_name $self.name:â $email ××××¥ ××¨×©××× $mlist.fqdn_listname $self.name: ×× × ××¦×× ××ª×××ª ×ª×§×¤× ××¨××©×× $self.name: ×× × ××¦×× ××ª××××ª ×ª×§×¤××ª ×××××× ×××× ×× $self.name: ××× ×¤×§××× ××××ª: $command_name $self.name: ×××ª×¨ ××× ××©×ª× ××: $printable_arguments (××× × ××©×) - ×¤×¨×× ×××××¢× ×××§××¨××ª: --send and --periodic flags cannot be used together <----- start object $count -----> A previous run of GNU Mailman did not exit cleanly ({}).  Try using --force ××× ×©×ª××× ×ª×××. ××§×× ××××¢×. ×××¡×¤×ª ×§×××××ª ×××××××ª ××¨×©××× ××¢×¨× ×××ª×¨×ª ×× ××©× (Subject). ×××¡×¤×ª ×××ª×¨××ª List-*â ××¤× ×ª×§× RFC 2369. ×××¡×¤×ª ×××××¢× ×××¨×××× ××. ×××¡×¤×ª ×××××¢× ××ª×§×¦××¨, ×¢×©××× ×× ××©××× ×××ª×. ××××¨ ×¡×× ×× ××ª×××, ×××××¢× × ××ª×¨× ×¨××§× Already subscribed (skipping): $display_name <$email> ×××¨ ×××¦×¢ ×¨××©×× (××××): $email ××× ×× ×Ö¾âendâ (×¡×××). ××× ×× ×Ö¾âjoinâ (××¦××¨×¤××ª). ××× ×× ×Ö¾âleaveâ (×¢××××). An alternative directory to output the various MTA files to. And you can print the list's request address by running:

% mailman withlist -r listaddr.requestaddr -l ant@example.com
Importing listaddr ...
Running listaddr.requestaddr() ...
ant-request@example.com ×¡×× ×× ×¨×©×××ª ×××××¨ ××××¨×××ª ××××. ××××ª ××¤×××ª× DMARC. As another example, say you wanted to change the display name for a particular
mailing list.  You could put the following function in a file called
`change.py`:

def change(mlist, display_name):
    mlist.display_name = display_name

and run this from the command line:

% mailman withlist -r change -l ant@example.com 'My List'

Note that you do not have to explicitly commit any database transactions, as
Mailman will do this for you (assuming no errors occured). ×ª×××× ××××××××ª ×××××¢× ×©×× ×× ×¨×©×××ª ××××××¨ â$display_nameâ ××¤×¨× ××¦× ×©×××: $value ×××©×× × ××¢× × ×××¢×××ª ×××¤××§××. ×××©×× ×× ××¢× ×× ××¨××××× ×©× ×××××¢×. ×× × ××ª× ××××× ××ª ××××× ×××¦×: $class_path Cannot parse as valid email address (skipping): $line Cannot unshunt message $filebase, skipping:
$error ××ª×¤××¡ ××××¢××ª ××××××ª ×××××× ×××¨×× ×©×¦×××. ××ª×¤××¡ ××××¢××ª ×¢× ××¢× ××¨×××. ××ª×¤××¡ ××××¢××ª ×©××× ××× ×××ª×¨××ª × ××©× (Subject) ×× ×©×× ×¨××§××ª. ×ª×¤××¡×ª ××××¢××ª ×¢× ×××ª×¨××ª ××©××××ª. ××ª×¤××¡ ××××¢××ª ×¢× ×××ª×¨ ××× × ××¢× ×× ××¤××¨×©××. ××ª×¤××¡ ×¤×§××××ª ××××´× ×× ××¢× ×©×××. ×××¡××¨ ×××ª×¨××ª ××¡×××××ª ××× ×××××¢××ª. ×××©××¨ ××§×©×ª ××¨×©××. × ×©××× ×× $person ××××¢×ª ×××××ª ×××××´× × ×©××× ××××¢×ª ××××´× ××××©××¨ ××¢×××× ×©× $person ×××¨×©××× $mlist.fqdn_listname ××¡×××× ××××××ª ×× ××ª××× ××© ×××©××¨ ××ª×¨××ª ×¡×× ×× ×ª××× ××××¢× Created mailing list: $mlist.fqdn_listname ×¤××§×× DMARC ×¢××¦×× ××××¢× ×¢× ×××ª×¨×ª ×¢×××× × ××ª××ª×× ×. ×××ª×¨×ª ×ª×§×¦××¨ ×ª××ª×× × ×××ª×¨×ª ×ª×§×¦××¨ ×¢×××× × ××©×××ª ×××××¢× ××¢×¦××¨×ª ××¢××××. ×¡×× ×× ×¨×©×××ª ×××××¨ ××××× ×× ×¢× ××¨×××× ×× ×¤×¨××××. Display Mailman's version. ××¦××ª ×¤××¨×× × ××¤×× ×©×××××ª × ××¡×£ ××§×××¥ ×××××. ×¤××××ª ×××©×ª× ×× ×©×× ××××¨×. ×¢× ××¨×©××× ××××ª ×××¤×¢× ×¤××§×× ×××¨×× ×¡××£ ×©×  ×¡×× ×× ×ª××× ×Ö¾MIME ×©× ××××¢××ª. ×××ª××¨ ×××× ×××ª ×Ö¾DMARC ×©× ×©× ××ª××× ××©×× From:â (×××ª). ××¡××××ª ×× ××××¢××ª, ×× × ××ª× ×××©×× × ×¢××× ×¨××©××ª.

×§×××¥ × ×¢×××: $config.LOCK_FILE
×××¨× × ×¢×××: $hostname

××ª×× ××ª ×××¦××ª. ××¢××¨× ×©× ××××¢× ××¤××§××ª GNU Mailman is already running GNU Mailman is in an unexpected state ($hostname != $fqdn_name) GNU Mailman is not running GNU Mailman is running (master pid: $pid) GNU Mailman is stopped (stale pid: $pid) Generating MTA alias maps ×§×××ª ×¨×©×××ª ×××¨× ××¨×©×××. ×§×××ª ×¢××¨× ×¢× ×¤×§××××ª ×××××´× ××××× ××ª. ×§×××ª ××××¢ ××§×××¥ ×ª××¨. ×§×××ª × ××¢× × ××§××× ××¨××××× ××§×××¥ ×××××. ××××ª×¨×ª â{}â ×ª××××ª ××©××¨× ×Ö¾bounce_matching_header (××××¨×ª ×××ª×¨×ª ×ª××××ª) ××××ª×¨×ª â{}â ×ª××××ª ×××× ×××ª×¨×ª Here's an example of how to use the --run option.  Say you have a file in the
Mailman installation directory called 'listaddr.py', with the following two
functions:

def listaddr(mlist):
    print(mlist.posting_address)

def requestaddr(mlist):
    print(mlist.request_address)

Run methods take at least one argument, the mailing list object to operate
on.  Any additional arguments given on the command line are passed as
positional arguments to the callable.

If -l is not given then you can run a function that takes no arguments.
 ×¢×××× ××××¢× ×××¤×¡×§×ª ××¢××××. ×¢×¦× ××ª×××× ×××××¢× ×× ×ª×× ×©×××¨× ×¢× ×××ª×¨×ª ×Ö¾Subject:â (× ××©×) ××¤× ×©×××,
×ª××¨×× ×Ö¾Mailman ×××ª×¢×× ××××××¢× ××××××§×ª. ×××××¥ ××¢×©××ª ×××ª ×× ×××ª
××××¢×ª ×¡×¤××. ×©××××ª ×ª×××× ×××××¢× ×× ×ª×× ×××××ª ××××ª×¨×ª Approved:â (××××©×¨)
×¢× ×¡×¡××ª ××¨×©××× ××ª×××, ×××××¢× ×ª×××©×¨ ××¤×¨×¡×× ××¨×©×××. ××××ª×¨×ª Approved:â
××××× ×× ××××¤××¢ ××©××¨× ××¨××©×× × ×©× ×××£ ××ª××××. Ignoring non-dictionary: {0!r} ×××ª×¢×× ×××××§×× ×©××× × ××§×¡×/MIME ×¤×©×× Illegal list name: $fqdn_listname Illegal owner addresses: $invalid Information about this Mailman instance. Inject a message from a file into a mailing list's queue. Invalid language code: $language_code ××ª×××ª ××××´× ×©×××× ×× ×××ª× ×××××ª×ª: $email Invalid value for [shell]use_python: {} Is the master even running? ××¦××¨×¤××ª ××¨×©×××ª ×××××¨ ××. ××ª×¨××ª ×ª×××× ××××××××ª ×××¨×× × ××××× ××¢××× ××ª ×¨×©×××ª ××××××¨ ×××. ××¢××× ××ª ×¨×©×××ª ××××××¨ ×××.

×××× ×××××ª ×©×××× ×¢××× ××××ª ××ª ××§×©×ª×. Less verbosity List all mailing lists. List already exists: $fqdn_listname List only those mailing lists that are publicly advertised ×××¦×× ××ª ×©×××ª ×××¦× ×× ××××× ×× ×××¦××ª. ×××¤××© ×××××ª ×¤×¨×¡××. ×××¤×© ××ª××× ×××× ×§××× ×××©××. ××ª×××ª ×× ×××××¢××ª ×©×¤××¨×¡×× ××¨×©×××ª ×××××¨ ×× ××××¢× ××ª
        ××§×××¦×ª ×××©××ª ××¤××§××ª.
         ××ª×××ª ××××¢××ª ×©× ×©××× ×××ª××××ª ××¡××××ª. ××ª×××ª ××××¢××ª ×©× ×©××××ª ×¢× ××× ×××¨×× ××¤××§××. ××ª×××ª ××××¢××ª ×©× ×©××××ª ×¢× ××× ×××× ×©××× × ×××¨××. ××ª×××ª ××××¢××ª ××× ×©××××× ×ª×§×¤××. Member not subscribed (skipping): $display_name <$email> Member not subscribed (skipping): $email ×××¨× ×¨×©×××ª ××××××¨ {}:
{} Membership is banned (skipping): $email ××××¢××ª ×©××××××ª ×¤×§××××ª × ×××××××ª ×××××¢× ××××ª ×××¨ ×¤××¨×¡×× ××¨×©××× ××××ª ×××××¢× ××© ××¢× ××¨××× ×××××¢× ××© ×××¢×× ×Ö¾{} × ××¢× ×× ×××××¢× ××× × ××©× ×××¢× ×××××¢× {} ××¡×× ××× ××¨×©××× ××××ª ×©×¨×©×¨×ª ×¤××§×× ×©×× ×× ×××ª×¨××ª ×××××¢×. ×××¢×××¨ ××ª ×××××¢× ××ª××¨ ××××©××ª ××××¦×××ª. ×× ×××× ××§×©×ª ××× ×× ×××©× ×× $self.mlist.display_name ×××ª $self.address.email ××§×©×ª ××××× ××× ×× ×××©× ×××¨×©××× $mlist.display_name ×××ª $email ××§×©×ª ××××× ××× ×× ×××©× ×××¨×©××× $self.mlist.display_name ×××ª $self.address.email No child with pid: $pid ×× × ××¦× ××¡×××× ×××××ª No matching mailing lists found ××× ××©×ª××© ×¨×©×× ×¢× ××ª×××ª ×××××´×: $email ×× ×¡××¤×§ ×©× ××¦×. ×× × ××¦×× ×©××××× ×××××¢×. ××× ×¤×§××× ××××ª: $command_name No such list found: $spec ××× ××¤×¨× ××× ××¨×©×××ª ×××××¨: $listspec No such list: $_list No such list: $listspec No such queue: $queue Not a Mailman 2.1 configuration file: $pickle_file Nothing to do Notify list owners/moderators of pending requests. Number of objects found (see the variable 'm'): $count Operate on digests. ×¡×× ×× ×¨×©×××ª ×××××¨ ×¨×××× ××××× ××. ××××¢× ××§××¨××ª PID unreadable in: $config.PID_FILE ×××¦××¢ ××××§××ª ×××××ª ARC (×©×¨×©×¨×ª ×××¢×× ××××× ×) ×××¦×¨×£ ××ª ××××ª×¨××ª ×××ª×§××××ª ×××¦×¢ ××××§××ª ×××××ª ×××¦×¨×£ ×××ª×¨×ª Authentication-Results (×ª××¦×××ª ×××××ª). ×¢×¨×××ª ×¤×¢××××××ª ×× ×××ª×××ª ××××¨ ×¤×¨×¡×× ×××¦××. Poll the NNTP server for messages to be gatewayed to mailing lists. ×¤×¨×¡×× ×× ××¢× ×§×××¦×ª ×××©××ª ××¤××§××ª ×¤×¨×¡×× ×××××¢× ×©×× ×¢× ×× ××©× â$subjectâ Print detailed instructions and exit. Print less output. Print some additional status. Print the Mailman configuration. ×¢×××× ×××××ª DMARC ×× ××ª×¢××××ª ××××¤×××ª×× ××¤××§ ×¨×©××× ×©× ×©×××ª ×××ª××××ª ×××××´× ×©× ××××¨××.

× ××ª× ×××©×ª××© ×××©×ª× × ××¨×©××ª ×× ××¡×¤×× delivery=â ×Ö¾mode=â ××× ×××××× ××ª
×××× ×××©×ª××©×× ×©×¢×× ×× ×¢× ××¦× ×/×× ×ª×¦××¨×ª ××©×××. ×× ×¦××× × delivery=â
×× mode=â ×××ª×¨ ××¤×¢× ×××ª, ×¨×§ ××¦××¨××£ ××××¨×× ××××©×.
 Programmatically, you can write a function to operate on a mailing list, and
this script will take care of the housekeeping (see below for examples).  In
that case, the general usage syntax is:

% mailman withlist [options] -l listspec [args ...]

where `listspec` is either the posting address of the mailing list
(e.g. ant@example.com), or the List-ID (e.g. ant.example.com). ×¡×××: {}

 Regenerate the aliases appropriate for your MTA. Regular expression requires --run ×××××ª/××§×¤×¦×ª ××××¢× ×××¤×¡×§×ª ××¢××××. ××¡×¨×ª ×××ª×¨××ª DomainKeys. ××¡×¨×ª ×¨×©×××ª ×××××¨. Removed list: $listspec Reopening the Mailman runners ×××§×©× ××¨×©×××ª ××××××¨ â$display_nameâ × ×××ª× Restarting the Mailman runners ×©××××ª ×××¨× ×¢× ×¤×¨×¡××. ×©××××ª ×ª×××××ª ×××××××××ª. ×©××××ª ×××××¢× ××ª××¨ ××××¦×. ×××¢×: {}
 Show a list of all available queue names and exit. Show also the list descriptions Show also the list names Show the current running status of the Mailman system. Show this help message and exit. Shutting down Mailman's master runner Signal the Mailman processes to re-open their log files. Stale pid file removed. Start the Mailman master and runner processes. Starting Mailman's master runner Stop and restart the Mailman runner subprocesses. ×¢×¦××¨×ª ×¢×××× ×¤×§××××ª. Stop the Mailman master and runner processes. × ××©×: {}
 ××§×©×ª ×¨××©×× ××××××§ ×¢××ª×§×× ××©×××¤××× ×©× ×××ª× ×××××¢×. ××ª×¢××××ª ×××××¢××ª ××¦× ×ª××× ××××¢××ª ×¢× ××ª×××ª × ××©×. ××× ××ª×××××ª ×©× Mailman ×××××¢× ×××¦××¨×¤×ª ×ª××××ª ××××× ×¡×× ×× ××ª××× ×©× ×¨×©×××ª ××××××¨
$mlist.display_name ×××¢××¨×ª× ××××¨× ×¨×©×××ª ××××××¨ × ×× ×¢×.
×××× ××¢××ª×§ ×××××× ×©× ××ª×¨ ××××××¢× ×©×××©×××.

 ×©×¨×©×¨×ª ××¤×¨×¡×× ×××¢××× (â-owner) ××××× ××ª. ×©×¨×©×¨×ª ××ª×××ª ××××ª×¨××ª ××××× ××ª ×©×¨×©×¨×ª ××¤××§×× ××××× ××ª. ×ª×¦××¨×ª ×××ª××¡×¨××ª ××××× ××ª ×××¢×××. ×ª×¦××¨×ª ××¤×¨×¡×× ××××× ××ª. ×¨×©×××ª ××××××¨ × ××¦××ª ×××¦× ××××§×ª ×××¨×× ××××××¢× ××××ª ××
        ×××©×¨× ××¨××© ×¢× ××× ×× ×××ª ××¨×©×××.
         ×× × ××ª× ×××©×× ××ª ×× ×¢××× ××¨××©××ª ××××× ×©× ×¨×× ×©××© ×××¨ ×©××¨××ª
×××¨× ×××¨ ××××. ×× × ××ª× ×××©×× × ×¢××× ×¨××©××ª ××××× ×©× ×¨×× ×× ×ª×××× ×××¨ ×× ×©×××¨× ×××¨ ××©×× ×××ª×.
××× ×× × ××¤×©×¨××ª ×××××§ × ×¢××××ª ××××©× ××ª ××¢××¨ ×××××××ª ××××¨×, ××× ×××× ×¢××× ×× ×§××ª
××ª ×× ×¢××× ××××ª ××× ××ª.

×§×××¥ × ×¢×××: $config.LOCK_FILE
×××¨× × ×¢×××: $hostname

××ª×× ××ª ×××¦××ª. ×× × ××ª× ×××©×× ××ª ×× ×¢××× ××¨×©×××ª. × ×¨×× ×©××© × ×¢××× ×¨××©××ª ××××©× ×ª. ×××× ×× ×¡××ª
×××¨××¥ ××ª $program ××××© ×¢× ×××× â--force. ×××××¢× ××××¢× ××××¨ ××¤××§×× ×××××¢× ××© ×××ª×¨×ª Approve (×××©××¨) ×× Approved (××××©×¨) ××ª××××. ×××××¢× ××× ×©××××× ×ª×§×¤×× ×××××¢× ××××× ××××××× ×××¨×××ª ×©× {} ×§×´× ×××××¢× ××× ××××¨ ××××¥ ××¨×©××× ×¡×× ××ª××× ×©× ×××××¢× × ××¡×¨ ×××¤××¨×© ×¡×× ××ª××× ×©× ×××××¢× ×× ×××©×¨ ×××¤××¨×© ×¡××××ª ××§×××¥ ×©×××××¢× × ××¡×¨× ×××¤××¨×© ×¡××××ª ××§×××¥ ×©×××××¢× ×× ×××©×¨× ×××¤××¨×© ×ª××¦×××ª ×¤×§××××ª ×××××´× ×©×× ×××¤××¢××ª ××××.
 ×ª××¦×××ª ×¤×§××××ª ×××××´× ×©×× ××©××× ××× × ××¨×©×××ª ××××¨×× {} The variable 'm' is the $listspec mailing list ×ª×¦××¨×ª ××¤×¨×¡×× ×××ª×××. ××¨×©××× {} ××© {} ××§×©××ª ×¤××§×× ×××ª×× ××ª. The {} list has {} moderation requests waiting.

{}
Please attend to this at your earliest convenience.
 There are two ways to use this script: interactively or programmatically.
Using it interactively allows you to play with, examine and modify a mailing
list from Python's interactive interpreter.  When running interactively, the
variable 'm' will be available in the global namespace.  It will reference the
mailing list object. This script provides you with a general framework for interacting with a
mailing list. × ××©×× ×××× ($count ××××¢××ª) × ××©×× ××××:
 ××××¢×ª ××××¨× ×©×× × ×ª×¤×¡× Undefined domain: $domain ×©× ×××¦× ×× ×××××¨: $name ××©×ª× ×× ×©××××× ×× ×× ××××¨××:
{} Unshunt messages. ××§×©×ª ××××× ××× ×× User: {}
 ××¨×× ×××× ×× ×¨×©×××ª ××××××¨ â$mlist.display_nameâ${digmode} ××¡××¨ ×× ××¤×¨×¡× ××¨×©××× ××××ª ××©× ×ª××× ×Ö¾From:â (×××ª) ×©××¤×¨×¡× ×××× ×××ª DMARC ×©× ××××× ×× ××××× ××××××¢× ×©×× × ×××ª× ××××××××ª. ×× ×××¢×ª× ×××××¢××ª ×©×× × ××××ª ××©×××, × × ×××¦××¨ ×§×©×¨ ×¢× ××¢×× ×¨×©×××ª ××××××¨ ×××ª×××ª ${listowner}. ××¡××¨ ×× ××¨×××ª ××ª ×¨×©×××ª ××××¨××. You can print the list's posting address by running the following from the
command line:

% mailman withlist -r listaddr -l ant@example.com
Importing listaddr ...
Running listaddr.listaddr() ...
ant@example.com ××××× ×ª ×××¦××¨×£ ××¨×©×××ª ××××××¨ $event.mlist.fqdn_listname. ×××× ×× ×©×× ××¨×©×××ª ××××××¨ $mlist.display_name ×××× ×ª××¤××¢ ××¤× ×× ××§×©× ×××©×¨ ××ª ××§×©×ª ×××× ×× ×©×× ××ª×× ×¤×§ ×¢×××¨× ×¡×¡×× ××× ××ª. 

×¢× ××× ×©××××© ×××¤×©×¨××ª âdigestâ (×ª×§×¦××¨), × ××ª× ××¦××× ××× ××¢× ××× ×××ª× ××§××
×ª×§×¦××¨ ×× ××. ×× ×× ×¦××× ×ª ×××¨, ×××¢×©× ×©××××© ××©×××ª ×××©××× ×××¨×¨×ª ×××××
×©× ×¨×©×××ª ××××××¨. × ××ª× ×××©×ª××© ×××¤×©×¨××ª âaddressâ (××ª×××ª) ××× ×××§×©
××× ×× ×©× ××ª×××ª ×©×× × ××× ×©××× × × ×©××× ×××××¢×.
 × ××¨×© ×××××ª ××¦×× ××× ×××¦××¨×£ ××¨×©×××ª ×××××¨ $event.mlist.fqdn_listname. × ××¨×© ×××××ª ××¦×× ××× ××¢××× ××ª ×¨×©×××ª ××××××¨ mailing list. ××××¢×ª× ×× $mlist.fqdn_listname ×××ª×× × ××××©××¨ ××¤××§×× ×¨×©×××ª ××××××¨ ××××©× ×©××: $fqdn_listname ×××× ×× ×©×× ××¨×©×××ª ××××××¨ ${mlist.display_name} ×××©××ª ×××××¢× ×××××¤× ×©×× ××¨×©×××ª ××××××¨ $mlist.display_name mailing ×× ×××©×¨×
×××©×××.  ××¦××¨×¤×ª ×××××¢× ×××§××¨××ª ×©××ª×§××× ×¢× ××× Mailman.
 [$mlist.display_name]  [----- end pickle -----] [----- start pickle -----] [ADD] %s [DEL] %s [×¤×¨×× ×××××¨× ××× × ×××× ××] [××× ×¤×¨××× ×××× ××] [×× ×¡××¤×§× ×¡×××] [×× ×¦××× × ×¡×××] ××©×ª× × ×©×××: $argument ×¨×©×××ª ××××××¨ â$listnameâ × ××¦×¨× ××¨××¢ ××©××××. ×××× ×¤×¨××× ××¡××¡××× ×¢×
×¨×©×××ª ××××××¨ ×©××.

××© ×× ×©×§ ××××¡×¡ ××××´× ×××©×ª××©×× (×× ××× ×××) ××¨×©××× ×©×× ××××¦×¢××ª× × ××ª×
××§×× ××××¢ ××¨× ×©××××ª ××××¢× ×¢× ×××× ××××× ×××× âhelpâ (×¢××¨×) ×× ××©× ×× ××××£
×××××¢× ××:

    $request_email

× × ×××¤× ××ª ××ª ×× ××©××××ª ×× $site_email. ×¢××¨× ×¢×××¨ ×¨×©×××ª ××××××¨ $listname

×× ×¤×§×××ª ×××××´× âhelpâ (×¢××¨×) ×¢×××¨ ××¨×¡× $version ×©× ×× ×× ××¨×©××××ª GNU Mailman
×ª××ª $domain.  ×××× ××ª×××¨××ª ×¤×§××××ª ×©× ××ª× ××©××× ××× ××§×× ××××¢ ×××©××× ×××× ××
×©×× ××¨×©××××ª ×©× Mailman ×××ª×¨ ×××.
×¤×§××× ×××× ×××××ª ××©××¨×ª ×× ××©× ×× ××××£ ×××××¢×.

××© ××©××× ××ª ××¤×§××××ª ×××ª×××ª â${listname}-request@${domain}.

×× ×××¢ ××ª××××¨×× - ××××× ××ª×× â<>â ××¦××× ××ª ×¤×¨×××× × ×××¦×× ××¢××
××××× ××ª×× â[]â ××¦××× ××ª ×¤×¨×× ×¨×©××ª.  ×× ××××× ××ª â<>â ×× ××ª
â[]â ××¢×ª ×©××××© ××¤×§××××ª.

××¤×§××××ª ×××××ª ×ª×§×¤××ª:

    $commands

×©××××ª ×××©×©××ª ×©×××¨×©×× ××¢× × ×× ××©× ××© ××©××× ××:

    $administrator ipython is not available, set use_ipython to no ××××§ ××× ×××ª ××¨×©×××, × ××¨×© ×××©××¨× ×××××ª ××¤×¨×¡×× ×××
××¨×©×××ª ××××××¨:

    ×¨×©×××:    $listname
    ×××ª:    $sender_email
    × ××©×: $subject

××××¢× ×× ××¢××××ª ×××¡×××:

$reasons

×××× × ××¤× ××, × × ×××§×¨ ×××× ×××§×¨× ×©×× ××× ×××©×¨ ×× ×××××ª
××ª ×××§×©×. × ××¨×© ×××©××¨× ××××©××¨ ××§×©×ª ××¦××¨×¤××ª ××××¨××ª ××¨×©×××ª ××××××¨:


    ×¢×××¨:  $member
    ×¨×©×××: $listname ×××©××¨× × ××¨×© ××× ×××©×¨ ××ª ××§×©×ª ××××× ×××× ×× ××¨×©×××ª ××××××¨:


    ×××××ª:  $member
    ×¨×©×××: $listname ×××× ×× ×©× $member ××¨×©×××ª ××××××¨ $listname ×××©××ª× ××××× ×©× ××§×× ×××××¨× ×××¨×
××Ö¾bounce_score_threshold (×¡×£ × ××§×× ××××¨×) ×©× ××¨×©×××. ×××× ×× ×©× $member ××¨×©×××ª ××××××¨ $listname ×××× ×¢×§× ××××¨××ª ×××××××ª. ×××× ×× ×©× $member ××¨×©××× $display_name ×××× ×××¦×××. ×××××¢× ×××¦××¨×¤×ª ××ª×§××× ×××××¨× ×× ×× ×©×ª×× ××ª ×××××¨× ×× ××××ª× ×× ×©××
××¦×××ª× ××××¥ ××× × ××ª××××ª ×©× ×××¨××.  ×¨×©×××ª ×××××¨ ×× ×××××¨× ××©××× ××ª ×× ××××¢××ª
×××××¨× ×©××× × ××××¢××ª ××× ×××ª ××¨×©×××. ×××¡×¨× ×©× $member ××¨×©×××ª ××××××¨ $display_name ×××©×××.   ××© ××©××× ××§×©××ª ××¨×©×××ª ××××××¨ $display_name ××
	$listname

××× ××××¨×©× ×× ×××× ×¨××©×× ××¨× ××××´×, ××© ××©××× ××××¢× ×¢× âhelpâ
(×¢××¨×) ×× ××©× ×× ×××£ ×××××¢× ××
	$request_email

× ××ª× ×××××¢ ××× ×××ª ××¨×©××× ×××ª×××ª
	$owner_email

××©××××ª ××ª×××× × × ××¢×¨×× ××ª ×©××¨×ª ×× ××©× ×©×× ××× ×©×× ×ª××¨×× ×××
â××¢× ×: ××ª××× ×©× ××ª×§×¦××¨ $display_nameâ¦â _______________________________________________
×¨×©×××ª ××××××¨$display_name â-- $listname
××× ×××× ××ª ×××× ×× ×¢××× ××©××× ××××¢×ª ××××´× ×× â${short_listname}-leave@${domain}â   ×××ª×××ª ×©×× "$user_email" ××××× × ×××¦××¨×£ ××¨×©×××ª ××××××¨
 $short_listname ×××ª×××ª $domain ×¢× ××× ××¢×× ×¨×©×××ª ××××××¨
$short_listname. × ××ª× ××××¢× ××ª ××××× × ×××¢× × ×××××¢× ××.

×× ×©× ××ª× ×× ××××× ××ª ×××©××¨× ××××ª -- ×××©××¨× ××××ª ××××
×××××¢× ×× $request_email:

    confirm $token

× × ××©×× ×× ×©×©××××ª `reply' (×ª××××) ×××××¢× ×× ××××¨×
××¢××× ×¢×××¨ ×¨×× ×§××¨×× ×××××´×.

×× ××¢××¤×ª× ××× ××¡×¨× ××××× ×, ××¤×©×¨ ×¤×©×× ×××ª×¢××
××××××¢× ××××ª. ×× ××© ×× ×©××××ª ××¤×©×¨ ××©××× ××××¢×
×× $owner_email. ×××©××¨ ×¨××©×× ××ª×××ª ××××´×

×©×××, ××× ×©×¨×ª ×Ö¾Mailman ××××ª GNU ×××ª×××ª $domain.

×§×××× × ××ª ××§×©×ª ××¨××©×× ×¢×××¨ ××ª×××ª ×××××´×

    $user_email

×××¨× ××ª××¤×©×¨ ×× ×××ª××× ××¢××× ×¢× Mailman ××××ª GNU ×××ª×¨ ××× ×ª××××
×¢××× ×××©×¨ ×©×××ª ××ª×××ª ×××××´× ×©××. × ××ª× ××¢×©××ª ×××ª ×¢× ××× ×©××××ª ×ª××××
×××××¢× ××××ª ×××× ××©× ××ª ××ª ×©××¨×ª ×× ××©× ×××.

×× ××× × ×¨××¦× ××¨×©×× ××ª ××ª×××ª ×××××´× ×××, × ××ª× ×××ª×¢×× ××××××¢×.
×× ×××¢×ª× ××§×©×ª ××¨××©×× ××¨×©××× ×××¦×¢× ×× ×××× ××¨×¦×× × ××××¤× ×××× × ×× ××
××© ×× ×©××××ª ×××©××, × ××ª× ×××¦××¨ ×§×©×¨:

    $owner_email ×××©××¨ ××××× ××× ×× ×©× ××ª×××ª ××××´×

×©×××, ××× ×©×¨×ª ×Ö¾GNU Mailman ×××ª×××ª $domain.

×§×××× × ××§×©×ª ××××× ××× ×× ×¢×××¨ ××ª×××ª ×××××´×

    $user_email

××¤× × ×©×Ö¾GNU Mailman ×ª××× ××¤×©×¨××ª ×××× ××ª ×××× ×× ×©××, ×ª×××× ×¢××× ×××©×¨
××ª ×××§×©× ×©××. × ××ª× ××¢×©××ª ×××ª ×¢× ××× ××¢× × ×××××¢× ×× ×ª×× ×©×××¨× ×¢× ×××ª×¨×ª
×× ××©× ××¤× ×©×××.

×× ××× ××¨×¦×× × ×××× ××ª ×××× ×× ×¢×××¨ ××ª×××ª ×××××´× ××× × ××ª× ×¤×©×× ×××ª×¢××
×××××¢× ××. ×× ×××¢×ª× ×××××¨ ×××××× ××× ×× ×©××§××¨× ×××¦×§× ×××× ××ª ×× ×× ××© ××
×©××××ª × ××¡×¤××ª, × ××ª× ×××¦××¨ ×§×©×¨ ××¨×

    $owner_email   ××××¢×ª ×××××´× ×©×× ×× â$listnameâ ×¢× ×× ××©×

    $subject

××¢××××ª ×¢× ×©×××¤× ××¤××§×× ×¢× ××¨×©××× ××××× ××¡×§××¨ ×××¨×××ª ×× × ××ª× ×××©×¨ ×××ª×.

×××××¢× ××¢××××ª ×××¡×××:

$reasons

×× ×©×××××¢× ×ª×¤××¨×¡× ××¨×©××× ×× ×©×ª××©×× ×××× ××××¢× ×¢× ×××××ª
×××¤×§×××. ×§×××× × ××××¢× ××××ª×××ª ×©×× <$sender_email> ×¢× ××§×©× ×××©××
××××××× ××¨×©×××ª ××××××¨ $listname.

×××¡×¤×¨ ×©×¨××× × ××××: $count. ××× ××××× ×¢ ×××¢×××ª ××× ××××××ª ××××´× ××× ×¨××××××
×©× ××××´×, ×× × ×©×× ×¢×× ×ª×××××ª ××××. × × ×× ×¡××ª ×©×× ×××¨.


×× ×××¢×ª× ×××××¨ ××©×××× ×× ×©××© ×× ×©××××ª, × × ×××¦××¨ ×§×©×¨ ×¢× ×× ×××ª ×¨×©×××ª
××××××¨ ×××ª×××ª $owner_email. ××××¢×ª× ×¢× ××××ª×¨×ª

    $subject

××ª×§××× ×××¦××× ×¢× ××× ×¨×©×××ª ××××××¨ $display_name. ×× ××××¢×ª ×ª×©×××. × ××ª× ×××ª×¢×× ××× ×.

×¨×©×××ª ××××××¨ $listname ×§×××× ××× ××¡×¤×¨ ×©× ××××¨××ª, ×× ×©××¦××× ×©×××¨×¢×
×ª×§×× ×××¢××¨×ª ××××¢××ª ×× $sender_email. ××¦××¨×¤×ª ××××× ××××. × × ×××§××¨ ××ª ×××××¢×
××× ××××× ×©××× ××¢×××ª ×××ª×××ª ×××××´×. ×××× ×××× ×× ××¤× ××ª ××× ×××ª ×××××´× ××¢××¨× × ××¡×¤×ª.

××× ×¦××¨× ××¤×¢××××ª ×××××××ª ××× ××××©××¨ ×××¦× ×××¨××ª ×¤×¢×× ××¨×©×××ª ××××××¨. 

×× ××© ×× ×©××××ª ×× ×ª×§×××ª, × ××ª× ×××¦××¨ ×§×©×¨ ×¢× ××¢×× ×¨×©×××ª ××××××¨ ×××ª×××ª

    $owner_email ××§×©×ª× ××¨×©×××ª ××××××¨ $listname

    $request

× ×××ª× ×¢× ××× ××× ×××¤×§×× ××¨×©×××.  ××§×©×ª× × ×××ª×
×¢× ×××¢×¨× ×××× ××¦× ××¤××§××:

â$reasonâ

× ××ª× ×××¤× ××ª ×©××××ª ×× ××¢×¨××ª ××©××¨××ª ×× ×× ×××ª ××¨×©×××
×××ª×××ª:

    $owner_email ××××¢×ª× ××¨×©×××ª ××××××¨ $listname × ×××ª× ×××¡××××ª ×××××ª:

$reasons


×××××¢× ×××§××¨××ª ×©××ª×§××× ×¢× ××× Mailman ××¦××¨×¤×ª ×××××¢× ××. ×××× ×× ×©×× ××¨×©×××ª ××××××¨ $listname ×××©××ª ××××× ×©×§××××ª ××¡×¤×¨ ×××× ×©× ××××¨××ª
×× ×©××¡×× ×©×× ×¨×× ××× ×× ××¢×××ª ×××¢××¨×ª ××××¢××ª ×× $sender_email. ×××××¥ ×××××§
×××ª ××× ×× ×××ª ×¡×¤×§ ×××××´× ×©×× ××§×××ª ×¡×××¢ × ××¡×£.


×× ××© ×× ×©××××ª ×× ××¢×××ª, × ××ª× ×××¦××¨ ×§×©×¨ ×¢× ××¢×× ×¨×©×××ª ××××××¨ ××¨×

    $owner_email ××¨×× ×××× ××¨×©×××ª ××××××¨ â$display_nameâ!

××× ××¤×¨×¡× ××¨×©××× ×××, ×¢××× ××©××× ××ª ××ª×××ª ×××××´× ×©×× ××:

  $listname

× ××ª× ×××× ××ª ×××× ×× ×× ××©× ××ª ××ª ××¤×©×¨××××ª ×©×× ××¨× ××××´× ×¢× ××× ×©××××ª ××××¢×
××:

  $request_email


×¢× ××××× âhelpâ (×¢××¨×) ×× ××©× ×× ××××£ ×××××¢× (××× ××××¨××××ª) ××ª××©×× ×××× ××××¢×
××××¨× ×¢× ×× ××××ª.  ××¦××¨× ×©×× ×× ×××¨×©×××ª ×ª×××¨×© ×× ×¡×¡××, ×× ×××¢×× ×××××
×¡×¡×× ×× ××× × × ××××ª ×××.  ×× ×©×××ª ××ª ××¡×¡×× ×©×× ×××× ×¢××× ×××¤×¡ ×××ª× ××¨×
×× ×©×§ ×××¤××¤×. ×× ×××× readline not available ××××ª×× ×××××× ×××××× ×××××ª ××¡×¤×¨×× ×©×××× ××××××××: $value 
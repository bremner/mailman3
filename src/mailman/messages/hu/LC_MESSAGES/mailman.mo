��    b      ,  �   <      H     I     R     ^     j     z     �     �     �     �     �     �  $   	  K   1	  6   }	  .   �	  -   �	  /   
  <   A
     ~
  !   �
  *   �
  4   �
          +     G     Y     m     �     �  "   �  	   �  *   �       &   !     H     c     �     �  #   �  "   �  !     %   #     I  (   a     �     �  #   �     �     �            M   #  =   q  O   �     �          8     M     e     s  )   �      �     �     �     �  0        <     V     b     �     �     �     �  "   �     �  e              �     �     �     �  	   �  ;   	  J   E  D   �  P   �  Q   &  >   x  J   �       !        ;     M     `     x     �     �  �  �  	   P     Z     w     �     �     �     �     �       $        =     R  Q   c  E   �  0   �  2   ,  1   _  S   �  0   �  2     .   I  5   x     �     �     �     �             (   9  1   b     �  '   �     �  8   �            6     W     l  8   �  <   �  (   �  &     "   F  '   i  #   �     �  #   �     �           '     H  `   L  5   �  `   �  "   D     g     �     �     �     �  &   �  '   �     $     0     G  D   f      �     �      �     �  +         B      N   '   f   !   �   Y   �      
!     #!  +   1!     ]!     |!     �!  A   �!  B   �!  9   +"  d   e"  Y   �"  F   $#  S   k#     �#  9   �#     $     %$     7$  h   S$  [   �$     %        
       0                     I      J          ?   !       V       a   %   R   _   `             (   ,   U         D   Y      A          7   2   #      [   	   X               W                  N   3       .   b   E   @   M                  '                 :   G   9       C   Z   >   O   =   5   "               -   S   4      ]          K   ;                        T       H      8   +   *       $   Q   )   6              ^      <      F            B   P   1       \   /   L   &    
- Done. 
- Ignored: 
- Results: 
- Unprocessed: 
Held Messages:
 
Held Subscriptions:
 
Held Unsubscriptions:
     Date: $date     From: $from_     Message-ID: $message_id     Subject: $subject $count matching mailing lists found: $member unsubscribed from ${mlist.display_name} mailing list due to bounces $member's subscription disabled on $mlist.display_name $mlist.display_name mailing list probe message $mlist.display_name subscription notification $mlist.display_name unsubscription notification $mlist.fqdn_listname post from $msg.sender requires approval $mlist.list_id has no members $person left $mlist.fqdn_listname $self.name: no such command: $command_name $self.name: too many arguments: $printable_arguments (no subject) - Original message details: Accept a message. An alias for 'end'. An alias for 'join'. An alias for 'leave'. Confirm a subscription request. Confirmation email sent to $person Confirmed Created mailing list: $mlist.fqdn_listname DMARC moderation Discard a message and stop processing. Display Mailman's version. Forward of moderated message GNU Mailman is already running GNU Mailman is not running Hold a message and stop processing. Ignoring non-text/plain MIME parts Illegal list name: $fqdn_listname Invalid language code: $language_code Join this mailing list. Last autoresponse notification for today Leave this mailing list. List all mailing lists. List already exists: $fqdn_listname Message has no subject Moderation chain Modify message headers. N/A New subscription request to $self.mlist.display_name from $self.address.email New unsubscription request from $mlist.display_name by $email New unsubscription request to $self.mlist.display_name from $self.address.email No such command: $command_name No such list found: $spec No such list: $_list No such list: $listspec Nothing to do Original Message Posting of your message titled "$subject" Print the Mailman configuration. Reason: {}

 Remove a mailing list. Removed list: $listspec Request to mailing list "$display_name" rejected Send automatic responses. Sender: {}
 Show also the list descriptions Show also the list names Stop processing commands. Subject: {}
 Subscription request The built-in header matching chain The built-in moderation chain. The master lock could not be acquired because it appears as though another
master is already running. Today's Topics ($count messages) Today's Topics:
 Uncaught bounce notification Undefined domain: $domain Unsubscription request User: {}
 Welcome to the "$mlist.display_name" mailing list${digmode} You have been invited to join the $event.mlist.fqdn_listname mailing list. You have been unsubscribed from the $mlist.display_name mailing list Your confirmation is needed to join the $event.mlist.fqdn_listname mailing list. Your confirmation is needed to leave the $event.mlist.fqdn_listname mailing list. Your message to $mlist.fqdn_listname awaits moderator approval Your subscription for ${mlist.display_name} mailing list has been disabled [$mlist.display_name]  [No bounce details are available] [No reason given] [No reasons given] bad argument: $argument list:admin:notice:disable.txt list:admin:notice:removal.txt n/a Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-01-18 11:48+0000
Last-Translator: balping <balping314@gmail.com>
Language-Team: Hungarian <https://hosted.weblate.org/projects/gnu-mailman/mailman/hu/>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.5-dev
 
- Kész. 
- Figyelmen kívül hagyva: 
- Eredmény: 
- Feldolgozatlan: 
Visszatartott üzenetek:
 
Visszatartott feliratkozók:
 
Visszatartott leiratkozások:
     Dátum: $date     Feladó: $from_     Üzenet azonosító: $message_id     Tárgy: $subject $count találat: $member lekerült a ${mlist.display_name} listáról visszapattanó levelek miatt A $member's előfizetése le van tiltva a $mlist.display_name oldalon $mlist.display_name levelezőlista próbaüzenet $mlist.display_name értesítés feliratkozásról $mlist.display_name értesítés leiratkozásról $mlist.fqdn_listname listára, $msg.sender által írt üzenet jóváhagyásra vár $mlist.list_id listának nincsenek feliratkozói $person elhagyta a(z) $mlist.fqdn_listname listát $self.name: nincs ilyen parancs: $command_name $self.name: túl sok argumentum: $printable_arguments (nincs tárgy) - Eredeti üzenet részletei: Üzenet elfogadása. Ugyanaz, mint az "end". Ugyanaz, mint a "join". Ugyanaz, mint a "leave". Feliratkozási kérvény jóváhagyása. Megerősítési email kiküldve $person részére Megerősítve Lista létrehozva: $mlist.fqdn_listname DMARC moderálás Üzenet megsemmisítése és feldolgozás leállítása. Mailman verzió megjelenítése. Moderált üzenet továbbítása GNU Mailman már fut GNU Mailman nem fut Üzenet felfüggesztése és feldolgozás leállítása. Nem sima szöveges / MIME részek figyelmen kívül hagyása Érvénytelen lista név: $fqdn_listname Érvénytelen nyelvkód $language_code Csatlakozás a levelezőlistához. Az utolsó mai automatikus válaszadás Leiratkozás a levelezőlistáról. Összes lista megjelenítése. Lista már létezik: $fqdn_listname Az üzenetnek nincs tárgya Moderálási lánc Üzenet fejlécek módosítása. N/A Új feliratkozási kérvény a(z) $self.mlist.display_name listára $self.address.email címről Új leiratkozási igény: $mlist.display_name, $email Új feliratkozási kérvény a(z) $self.mlist.display_name listára $self.address.email címről Nincs ilyen parancs: $command_name Nincs ilyen lista: $spec Nincs ilyen lista: $_list Nincs ilyen lista: $listspec Nincs teendő Eredeti üzenet "$subject" tárgyú üzenete küldése Mailman beállítások megjelenítése. Indok: {}

 Lista eltávolítása. Lista eltávolítva: $listspec "$display_name" levelezőlistára küldött lekérdezés elutasítva Automatikus válaszok küldése. Feladó: {}
 Lista leírások megjelenítése Lista nevek megjelenítése Parancsok feldolgozásának megállítása. Tárgy: {}
 Feliratkozási kérelem A beépített fejléccazonsoító lánc A beépített moderálási lánc. A mesterzárat nem lehet kinyerni, mert úgy tűnik, hogy
egy másik masterzár már fut. Mai témák ($count db.) Mai témák:
 Észrevétlen visszafordulási értesítés Nem definiált domain: $domain Leiratkozási kérelem Felhasználó: {}
 Üdvözöljük a "$mlist.display_name" levelezőlistán${digmode} Meg lett hívva a(z) $event.mlist.fqdn_listname levelezőlistára. Leiratkozott a(z) $mlist.display_name levelezőlistáról Az ön hozzájárulása szükséges a(z) $event.mlist.fqdn_listname listához való csatlakozáshoz. Az ön hozzájárulása szülséges a(z) $event.mlist.fqdn_listname lista elhagyásához. A(z) $mlist.fqdn_listname listára küldött levele moderálásra vár A(z) {mlist.display_name} levelezési listára szóló feliratkozása le van tiltva [$mlist.display_name]  [A visszapattanás részletei nem állnak rendelkezésre] [indoklás nélkül] [Nincs indoklás] rossz argumentum: $argument $member feliratkozása fel lett függesztve a(z) $listname listán túl sok visszapattanó levél miatt. $member el lett távolítva a(z) $listname listáról túl sok visszapattanó levél miatt. n/a 
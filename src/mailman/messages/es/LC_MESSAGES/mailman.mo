��    u     �  �  l      8  �  9  a  0!     �#     �#     �#     �#     �#     �#     �#  T   $  �  W$  �   &    �&  �   �(  �   �)  �   P*     K+  }   [+  �   �+  '   �,  �   �,  �   �-  Y   I.  �   �.  ^   }/  \   �/  $  90  Y   ^1  o   �1  _   (2  G   �2     �2  q  �2  �  S4  �  66  �   8  �   �8  �   T9  �   �9  �   m:    �:     ?  y   1?  �  �?  P   �B  I   �B  �   &C  �   D  �   �D  :   �E  P   �E  J   MF  9   �F  �   �F  �   �G  {   wH  b   �H  ^   VI  R   �I  �   J  �   �J  g   �K  >  *L  �   iM  �  N  q   �P     (Q  I   >Q  �   �Q  �   /R    S  n   T  ,  �T  �   �U  �   lV  �  XW  :   XY     �Y  $   �Y  !   �Y  K   �Y  6   5Z  =   lZ  .   �Z  -   �Z  /   [  <   7[  Q   t[     �[  M   �[  L   2\     \  0   �\  !   �\  !   �\  :   
]  /   E]  7   u]  *   �]  4   �]     ^     ^  3   6^  !   j^  K   �^     �^     �^  7   _      ?_      `_  3   �_  .   �_  5   �_  %   `     @`     T`     i`  <   `  �   �`  !   �a     �a  �  �a  B   �c     �c  -   �c  0   d  (   Md  5   vd  2   �d  8   �d  )   e  2   Be  '   ue  1   �e  #   �e  *   �e     f  "   >f  @   af      �f  	   �f  #   �f  *   �f     g  ,   -g     Zg     hg  &   vg  4   �g     �g  3   �g     !h  /   ;h     kh  $   sh  "   �h  x   �h     4i     Qi  ?   pi     �i  )   �i  (   �i     j     8j  (   Xj  $   �j  8   �j  1   �j  !   k    3k  #   Km  m  om     �n  "   �n  !   o  !   Ao  (   co  9   �o  %   �o  +   �o  '   p     @p     \p  (   tp     �p  C   �p     �p     	q  #   !q  :   Eq  )   �q     �q     �q  d   �q  (   Hr  )   qr  "   �r  %   �r  8   �r  (   s  "   Fs  '   is     �s  ,   �s      �s  #   �s     "t  *   9t     dt     ut  ,   �t     �t  M   �t  =   u  O   Ju     �u     �u     �u  ,   �u     v  #   1v     Uv     tv  %   �v     �v     �v     �v  2   �v     *w  2   8w  6   kw     �w  '   �w     �w  #   �w  4   x  =   Hx  1   �x  C   �x  %   �x  )   "y  %   Ly     ry     �y      �y  +   �y  #  �y  y  {     �|  0   �|  !   �|  ,   �|     }     6}     M}     e}  0   �}     �}  $   �}     �}  '   ~     :~  2   F~     y~     �~  6   �~      �~  %   
  8   0     i  .   �      �  1   �     �  -   �     K�     X�  -   m�     ��      ��     Հ  �   �  "   ρ  "   �     �     4�     Q�  w   p�  e   �    N�  �   b�  )   �  6   �      N�  1   o�  %   ��  4   ǅ  5   ��  6   2�  7   i�  6   ��  "   ؆  &   ��  .   "�     Q�  /   l�  h   ��  G  �  V   M�      ��     ŉ     ։     �     �  '   *�     R�     d�  	   {�  ;   ��    ��  2   ً  �   �  J   ߌ  D   *�  u  o�  P   �  Q   6�  >   ��  %   Ǐ  J   �  �   8�     А     �      �     �     $�  !   -�     O�     j�     |�     ��      ��     ȑ  /   ё     �     �  !   <�     ^�     |�     ��  "   ��  !   ݒ     ��     �     =�     \�     {�     ��      ��     ד     ��  "   �     1�     K�     f�     ��     ��     ��     ڔ     ޔ  (   ��  �  �     ��  �  ��  	   f�     p�     ~�     ��     ��     ��     ϛ  i   �  �  W�  �   N�  /  F�  �   v�  �   $�    #�     A�  �   R�    ֤  -   �  �   �  �   ɦ  a   ��    �  [   �  V   H�  -  ��  t   ͪ  u   B�  b   ��  R   �     n�  �  }�  B  ,�  B  o�  �   ��  �   c�  �   *�  �   ڴ  �   ~�  �  �     �  ~   	�  M  ��  \   ־  P   3�  �   ��  �   c�  �   D�  C   B�  a   ��  [   ��  F   D�  �   ��    ��  �   ��  l   !�  v   ��  d   �  �   j�  (  H�  n   q�  x  ��  �   Y�  �  <�  �   :�     ��  b   ��  �   5�    ��    ��  y   �  '  ��  �   ��  Z  ��  i  ��  E   f�     ��  5   ��  %   ��  _   �  B   x�  @   ��  8   ��  3   5�  ,   i�  F   ��  T   ��      2�  \   S�  Q   ��     �  7   �  "   Q�  *   t�  8   ��  G   ��  O    �  -   p�  7   ��     ��  "   ��  ;   �  %   B�  W   h�     ��     ��  J   ��  &   =�  "   d�  8   ��  :   ��  /   ��     +�     K�     `�     z�  ?   ��  �   ��  .   ��     ��    ��  J   �  .   Z�  ;   ��  0   ��  5   ��  I   ,�  ?   v�  >   ��  )   ��  /   �  +   O�  ;   {�  *   ��  0   ��  '   �  )   ;�  L   e�  (   ��  
   ��  /   ��  ,   �     C�  3   U�     ��     ��  .   ��  >   ��     1�  C   Q�     ��  ;   ��     ��  *   ��  +   �  �   I�     ��  "   ��  C   �  "   _�  9   ��  -   ��  $   ��  /   �  7   ?�  +   w�  J   ��  E   ��  1   4�  X  f�  +   ��  �  ��     ��  2   ��  &   ��  .   ��  -   +�  D   Y�  ,   ��  7   ��  +   �     /�     O�  -   n�     ��  F   ��     ��  "   �  "   0�  >   S�  9   ��      ��  -   ��  �   �  6   ��  5   ��  .   �  -   ;�  7   i�  '   ��     ��  ?   ��  !   )�  +   K�  4   w�  )   ��     ��  6   ��     (�      >�  0   _�     ��  T   ��  9   ��  L   #�     p�  2   ��  8   ��  >   ��  2   6�  0   i�  $   ��     ��  ;   ��     �     4�     P�  ?   h�     ��  R   ��  <   
�     G�  2   ]�     ��  "   ��  S   ��  \   �  8   u�  G   ��  6   ��  .   -�  )   \�     ��      ��  %   ��  4   ��  B    �  [    � 1   � $   + 7   P     �    �    � $   � 5    %   = &   c    � &   �    � C   � ,   $ &   Q ;   x '   � +   � U       ^ 5   | (   � :   � %    5   <    r    ~ -   �    � .   � %    �   7 /   	 3   G	 %   {	 #   �	    �	 �   �	 a   r
 N  �
 �   # *   � <   � '   & 3   N *   � E   � C   � G   7 G    H   � .    0   ? 5   p    � 9   � d   � Y  a U   �        0 "   A    d &   � +   �    �    �    � ?   
 +  J 7   v �   � N   � ?   � �  ! e   � Z    E   s (   � L   � �   /    �    �    �         (   *    S    q    �    � �  � 2  f 4   � 7  � w   ! w   ~! �   �! X   �" 5   �"   # +   ,$    X$ �  Z$ �   �%    �& :  �& 0  �( h  ,    l/ 0  n/ �  �0 `   m2 �  �2   R5 �   l6 �  7 G  �8    �:    �: -   ;    �   �      �   u   \  \       U  (   y   �   H  {       1      �   6   �       �   Q      H   L  c  �       �   �   �       +      5   u  p  �   4   b       &  i  W     ,          �   R    |       �         �      �          �   �       �         �   b  �   E   <       �     �   L   �   I  �   i           �   N  `          �   �   }   h  r  =      7    �   5      P     B   Z         @  �   �   >   D  �       �       n  �   s  Y           �      �         o  �       :   3  %  !   �   <  t  �   �   ^  A  �                       �   e  #  �   �      m   N   �       �               C      ?  .  [  R     �       �   d           �   #   �           X              �       �       8     V    �   �   f  �   ^   l   �       !  �             J   $   �   �   �           �       �       �   �   3           d    �       7   Z       -  �   �   S   x       �   �   �          *     v   �   j  h   :     
      o       �   �   �     9   %   +       P  �   �       (  �   2   4  ,   j   [   �   E    -         �   `   �   �       q   e     �       �   ?   "       �   �   k   "     F      V     2  �       	      >  �   1       D   �   s       �      l          ;   �   q  /   �   *          t              O  c   _  �   �       8       �       a  9  �       C      �   �                 z                w   �   �   K      �       0   m  M  �   �   f   �   �           .   �       n   G         �       �   �   =           ;  �   U          p   M       K  @   �   T            I   )   �               �   W           '      �   a           	       ~           /  &               F   �       G   _       )  A   r   �           S      B     T              �       k  �                    Y  '   �       6  O     �   �   �   X      J      �   �   �               g      $  0  �   �              ]  
  ]      �   g   Q   �    
    Run a script.  The argument is the module path to a callable.  This
    callable will be imported and then, if --listspec/-l is also given, is
    called with the mailing list as the first argument.  If additional
    arguments are given at the end of the command line, they are passed as
    subsequent positional arguments to the callable.  For additional help, see
    --details.

    If no --listspec/-l argument is given, the script function being called is
    called with no arguments.
     
    Start the named runner, which must be one of the strings returned by the -l
    option.

    For runners that manage a queue directory, optional `slice:range` if given
    is used to assign multiple runner processes to that queue.  range is the
    total number of runners for the queue while slice is the number of this
    runner from [0..range).  For runners that do not manage a queue, slice and
    range are ignored.

    When using the `slice:range` form, you must ensure that each runner for the
    queue is given the same range value.  If `slice:runner` is not given, then
    1:1 is used.
     
- Done. 
- Ignored: 
- Results: 
- Unprocessed: 
Held Messages:
 
Held Subscriptions:
 
Held Unsubscriptions:
     A more verbose output including the file system paths that Mailman is
    using.     A specification of the mailing list to operate on.  This may be the posting
    address of the list, or its List-ID.  The argument can also be a Python
    regular expression, in which case it is matched against both the posting
    address and List-ID of all mailing lists.  To use a regular expression,
    LISTSPEC must start with a ^ (and the matching is done with re.match().
    LISTSPEC cannot be a regular expression unless --run is given.     Add all member addresses in FILENAME with delivery mode as specified
    with -d/--delivery.  FILENAME can be '-' to indicate standard input.
    Blank lines and lines that start with a '#' are ignored.
         Add and delete members as necessary to syncronize a list's membership
    with an input file.  FILENAME is the file containing the new membership,
    one member per line.  Blank lines and lines that start with a '#' are
    ignored.  Addresses in FILENAME which are not current list members
    will be added to the list with delivery mode as specified with
    -d/--delivery.  List members whose addresses are not in FILENAME will
    be removed from the list.  FILENAME can be '-' to indicate standard input.
         Additional metadata key/value pairs to add to the message metadata
    dictionary.  Use the format key=value.  Multiple -m options are
    allowed.     Configuration file to use.  If not given, the environment variable
    MAILMAN_CONFIG_FILE is consulted and used if set.  If neither are given, a
    default configuration file is loaded.     Create a mailing list.

    The 'fully qualified list name', i.e. the posting address of the mailing
    list is required.  It must be a valid email address and the domain must be
    registered with Mailman.  List names are forced to lower case.     Date: $date     Delete all the members of the list.  If specified, none of -f/--file,
    -m/--member or --fromall may be specified.
         Delete list members whose addresses are in FILENAME in addition to those
    specified with -m/--member if any.  FILENAME can be '-' to indicate
    standard input.  Blank lines and lines that start with a '#' are ignored.
         Delete members from a mailing list.     Delete the list member whose address is ADDRESS in addition to those
    specified with -f/--file if any.  This option may be repeated for
    multiple addresses.
         Delete the member(s) specified by -m/--member and/or -f/--file from all
    lists in the installation.  This may not be specified together with
    -a/--all or -l/--list.
         Discard all shunted messages instead of moving them back to their original
    queue.     Display, add or delete a mailing list's members.
    Filtering along various criteria can be done when displaying.
    With no options given, displaying mailing list members
    to stdout is the default mode.
         Don't actually do anything, but in conjunction with --verbose, show what
    would happen.     Don't actually make the changes.  Instead, print out what would be
    done to the list.     Don't attempt to pretty print the object.  This is useful if there is some
    problem with the object and you just want to get an unpickled
    representation.  Useful with 'mailman qfile -i <file>'.  In that case, the
    list of unpickled objects will be left in a variable called 'm'.     Don't print status messages.  Error messages are still printed to standard
    error.     Don't restart the runners when they exit because of an error or a SIGUSR1.
    Use this only for debugging.     File to send the output to.  If not given, or if '-' is given, standard
    output is used.     File to send the output to.  If not given, standard output is used.     From: $from_     Generate the MTA alias files upon startup. Some MTA, like postfix, can't
    deliver email if alias files mentioned in its configuration are not
    present. In some situations, this could lead to a deadlock at the first
    start of mailman3 server. Setting this option to true will make this
    script create the files and thus allow the MTA to operate smoothly.     If the master watcher finds an existing master lock, it will normally exit
    with an error message.  With this option, the master will perform an extra
    level of checking.  If a process matching the host/pid described in the
    lock file is running, the master will still exit, requiring you to manually
    clean up the lock.  But if no matching process is found, the master will
    remove the apparently stale lock and make another attempt to claim the
    master lock.     If the master watcher finds an existing master lock, it will normally exit
    with an error message.  With this option,the master will perform an extra
    level of checking.  If a process matching the host/pid described in the
    lock file is running, the master will still exit, requiring you to manually
    clean up the lock.  But if no matching process is found, the master will
    remove the apparently stale lock and make another attempt to claim the
    master lock.     Import Mailman 2.1 list data.  Requires the fully-qualified name of the
    list to import and the path to the Mailman 2.1 pickle file.     Increment the digest volume number and reset the digest number to one.  If
    given with --send, the volume number is incremented before any current
    digests are sent.     Key to use for the lookup.  If no section is given, all the key-values pair
    from any section matching the given key will be displayed.     Leaves you at an interactive prompt after all other processing is complete.
    This is the default unless the --run option is given.     List only those mailing lists hosted on the given domain, which
    must be the email host name.  Multiple -d options may be given.
         Master subprocess watcher.

    Start and watch the configured runners, ensuring that they stay alive and
    kicking.  Each runner is forked and exec'd in turn, with the master waiting
    on their process ids.  When it detects a child runner has exited, it may
    restart it.

    The runners respond to SIGINT, SIGTERM, SIGUSR1 and SIGHUP.  SIGINT,
    SIGTERM and SIGUSR1 all cause a runner to exit cleanly.  The master will
    restart runners that have exited due to a SIGUSR1 or some kind of other
    exit condition (say because of an uncaught exception).  SIGHUP causes the
    master and the runners to close their log files, and reopen then upon the
    next printed message.

    The master also responds to SIGINT, SIGTERM, SIGUSR1 and SIGHUP, which it
    simply passes on to the runners.  Note that the master will close and
    reopen its own log files on receipt of a SIGHUP.  The master also leaves
    its own process id in the file specified in the configuration file but you
    normally don't need to use this PID directly.     Message-ID: $message_id     Name of file containing the message to inject.  If not given, or
    '-' (without the quotes) standard input is used.     Normally, this script will refuse to run if the user id and group id are
    not set to the 'mailman' user and group (as defined when you configured
    Mailman).  If run as root, this script will change to this user and group
    before the check is made.

    This can be inconvenient for testing and debugging purposes, so the -u flag
    means that the step that sets and checks the uid/gid is skipped, and the
    program is run as the current user and group.  This flag is not recommended
    for normal production environments.

    Note though, that if you run with -u and are not in the mailman group, you
    may have permission problems, such as being unable to delete a list's
    archives through the web.  Tough luck!     Notify the list owner by email that their mailing list has been
    created.     Operate on a mailing list.

    For detailed help, see --details
         Operate on this mailing list.  Multiple --list options can be given.  The
    argument can either be a List-ID or a fully qualified list name.  Without
    this option, operate on the digests for all mailing lists.     Operate on this mailing list.  Multiple --list options can be given.  The
    argument can either be a List-ID or a fully qualified list name.  Without
    this option, operate on the requests for all mailing lists.     Override the default set of runners that the master will invoke, which is
    typically defined in the configuration file.  Multiple -r options may be
    given.  The values for -r are passed straight through to bin/runner.     Override the list's setting for admin_notify_mchanges.     Override the list's setting for send_goodbye_message to
    deleted members.     Override the list's setting for send_welcome_message to added members.     Override the list's setting for send_welcome_message.     Register the mailing list's domain if not yet registered.  This is
    the default behavior, but these options are provided for backward
    compatibility.  With -D do not register the mailing list's domain.     Run the named runner exactly once through its main loop.  Otherwise, the
    runner runs indefinitely until the process receives a signal.  This is not
    compatible with runners that cannot be run once.     Section to use for the lookup.  If no key is given, all the key-value pairs
    of the given section will be displayed.     Send any collected digests for the List only if their digest_send_periodic
    is set to True.     Send any collected digests right now, even if the size threshold has not
    yet been met.     Send the added members an invitation rather than immediately adding them.
         Set the added members delivery mode to 'regular', 'mime', 'plain',
    'summary' or 'disabled'.  I.e., one of regular, three modes of digest
    or no delivery.  If not given, the default is regular.     Set the added members delivery mode to 'regular', 'mime', 'plain',
    'summary' or 'disabled'.  I.e., one of regular, three modes of digest
    or no delivery.  If not given, the default is regular.  Ignored for invited
    members.     Set the list's preferred language to CODE, which must be a registered two
    letter language code.     Specify a list owner email address.  If the address is not currently
    registered with Mailman, the address is registered and linked to a user.
    Mailman will send a confirmation message to the address, but it will also
    send a list creation notice to the address.  More than one owner can be
    specified.     Specify the encoding of strings in PICKLE_FILE if not utf-8 or a subset
    thereof. This will normally be the Mailman 2.1 charset of the list's
    preferred_language.     Start a runner.

    The runner named on the command line is started, and it can either run
    through its main loop once (for those runners that support this) or
    continuously.  The latter is how the master runner starts all its
    subprocesses.

    -r is required unless -l or -h is given, and its argument must be one of
    the names displayed by the -l switch.

    Normally, this script should be started from `mailman start`.  Running it
    separately or with -o is generally useful only for debugging.  When run
    this way, the environment variable $MAILMAN_UNDER_MASTER_CONTROL will be
    set which subtly changes some error handling behavior.
         Start an interactive Python session, with a variable called 'm'
    containing the list of unpickled objects.     Subject: $subject     The list to operate on.  Required unless --fromall is specified.
         The name of the queue to inject the message to.  QUEUE must be one of the
    directories inside the queue directory.  If omitted, the incoming queue is
    used.     [MODE] Add all member addresses in FILENAME.  FILENAME can be '-' to
    indicate standard input.  Blank lines and lines that start with a
    '#' are ignored.  This option is deprecated in favor of 'mailman
    addmembers'.     [MODE] Delete all member addresses found in FILENAME
    from the specified list. FILENAME can be '-' to indicate standard input.
    Blank lines and lines that start with a '#' are ignored.
    This option is deprecated in favor of 'mailman delmembers'.     [MODE] Display output to FILENAME instead of stdout.  FILENAME
    can be '-' to indicate standard output.     [MODE] Synchronize all member addresses of the specified mailing list
    with the member addresses found in FILENAME.
    FILENAME can be '-' to indicate standard input.
    Blank lines and lines that start with a '#' are ignored.
    This option is deprecated in favor of 'mailman syncmembers'.     [output filter] Display only digest members of kind.
    'any' means any digest type, 'plaintext' means only plain text (rfc 1153)
    type digests, 'mime' means MIME type digests.     [output filter] Display only members with a given ROLE.
    The role may be 'any', 'member', 'nonmember', 'owner', 'moderator',
    or 'administrator' (i.e. owners and moderators).
    If not given, then delivery members are used.      [output filter] Display only members with a given delivery status.
    'enabled' means all members whose delivery is enabled, 'any' means
    members whose delivery is disabled for any reason, 'byuser' means
    that the member disabled their own delivery, 'bybounces' means that
    delivery was disabled by the automated bounce processor,
    'byadmin' means delivery was disabled by the list
    administrator or moderator, and 'unknown' means that delivery was disabled
    for unknown (legacy) reasons.     [output filter] Display only regular delivery members.  (Digest mode) $count matching mailing lists found: $display_name post acknowledgment $member unsubscribed from ${mlist.display_name} mailing list due to bounces $member's subscription disabled on $mlist.display_name $mlist.display_name Digest, Vol $volume, Issue $digest_number $mlist.display_name mailing list probe message $mlist.display_name subscription notification $mlist.display_name unsubscription notification $mlist.fqdn_listname post from $msg.sender requires approval $mlist.list_id bumped to volume $mlist.volume, number ${mlist.next_digest_number} $mlist.list_id has no members $mlist.list_id is at volume $mlist.volume, number ${mlist.next_digest_number} $mlist.list_id sent volume $mlist.volume, number ${mlist.next_digest_number} $name runs $classname $person has a pending subscription for $listname $person left $mlist.fqdn_listname $realname via $mlist.display_name $self.name: $email is not a member of $mlist.fqdn_listname $self.name: No valid address found to subscribe $self.name: No valid email address found to unsubscribe $self.name: no such command: $command_name $self.name: too many arguments: $printable_arguments (no subject) - Original message details: --send and --periodic flags cannot be used together <----- start object $count -----> A previous run of GNU Mailman did not exit cleanly ({}).  Try using --force A rule which always matches. Accept a message. Add a list-specific prefix to the Subject header value. Add the RFC 2369 List-* headers. Add the message to the archives. Add the message to the digest, possibly sending it. After content filtering, the message was empty Already subscribed (skipping): $display_name <$email> Already subscribed (skipping): $email An alias for 'end'. An alias for 'join'. An alias for 'leave'. An alternative directory to output the various MTA files to. And you can print the list's request address by running:

% mailman withlist -r listaddr.requestaddr -l ant@example.com
Importing listaddr ...
Running listaddr.requestaddr() ...
ant-request@example.com Announce only mailing list style. Apply DMARC mitigations. As another example, say you wanted to change the display name for a particular
mailing list.  You could put the following function in a file called
`change.py`:

def change(mlist, display_name):
    mlist.display_name = display_name

and run this from the command line:

% mailman withlist -r change -l ant@example.com 'My List'

Note that you do not have to explicitly commit any database transactions, as
Mailman will do this for you (assuming no errors occured). Auto-response for your message to the "$display_name" mailing list Bad runner spec: $value Calculate the owner and moderator recipients. Calculate the regular recipients of the message. Cannot import runner module: $class_path Cannot parse as valid email address (skipping): $line Cannot unshunt message $filebase, skipping:
$error Catch messages that are bigger than a specified maximum. Catch messages with implicit destination. Catch messages with no, or empty, Subject headers. Catch messages with suspicious headers. Catch messages with too many explicit recipients. Catch mis-addressed email commands. Cleanse certain headers from all messages. Confirm a subscription request. Confirmation email sent to $person Confirmation email sent to $person to leave $mlist.fqdn_listname Confirmation token did not match Confirmed Content filter message notification Created mailing list: $mlist.fqdn_listname DMARC moderation Decorate a message with headers and footers. Digest Footer Digest Header Discard a message and stop processing. Discussion mailing list style with private archives. Display Mailman's version. Display more debugging information to the log file. Echo back your arguments. Emergency moderation is in effect for this list End of  Filter the MIME content of messages. Find DMARC policy of From: domain. For unknown reasons, the master lock could not be acquired.

Lock file: $config.LOCK_FILE
Lock host: $hostname

Exiting. Forward of moderated message GNU Mailman is already running GNU Mailman is in an unexpected state ($hostname != $fqdn_name) GNU Mailman is not running GNU Mailman is running (master pid: $pid) GNU Mailman is stopped (stale pid: $pid) Generating MTA alias maps Get a list of the list members. Get help about available email commands. Get information out of a queue file. Get the normal delivery recipients from an include file. Header "{}" matched a bounce_matching_header line Header "{}" matched a header rule Here's an example of how to use the --run option.  Say you have a file in the
Mailman installation directory called 'listaddr.py', with the following two
functions:

def listaddr(mlist):
    print(mlist.posting_address)

def requestaddr(mlist):
    print(mlist.request_address)

Run methods take at least one argument, the mailing list object to operate
on.  Any additional arguments given on the command line are passed as
positional arguments to the callable.

If -l is not given then you can run a function that takes no arguments.
 Hold a message and stop processing. If you reply to this message, keeping the Subject: header intact, Mailman will
discard the held message.  Do this if the message is spam.  If you reply to
this message and include an Approved: header with the list password in it, the
message will be approved for posting to the list.  The Approved: header can
also appear in the first line of the body of the reply. Ignoring non-dictionary: {0!r} Ignoring non-text/plain MIME parts Illegal list name: $fqdn_listname Illegal owner addresses: $invalid Information about this Mailman instance. Inject a message from a file into a mailing list's queue. Invalid language code: $language_code Invalid or unverified email address: $email Invalid value for [shell]use_python: {} Is the master even running? Join this mailing list. Last autoresponse notification for today Leave this mailing list. Leave this mailing list.

You may be asked to confirm your request. Less verbosity List all mailing lists. List already exists: $fqdn_listname List only those mailing lists that are publicly advertised List the available runner names and exit. Look for a posting loop. Look for any previous rule hit. Match all messages posted to a mailing list that gateways to a
        moderated newsgroup.
         Match messages sent by banned addresses. Match messages sent by moderated members. Match messages sent by nonmembers. Match messages with no valid senders. Member not subscribed (skipping): $display_name <$email> Member not subscribed (skipping): $email Members of the {} mailing list:
{} Membership is banned (skipping): $email Message contains administrivia Message has already been posted to this list Message has implicit destination Message has more than {} recipients Message has no subject Message sender {} is banned from this list Moderation chain Modify message headers. Move the message to the outgoing news queue. N/A New subscription request to $self.mlist.display_name from $self.address.email New unsubscription request from $mlist.display_name by $email New unsubscription request to $self.mlist.display_name from $self.address.email No child with pid: $pid No confirmation token found No matching mailing lists found No registered user for email address: $email No runner name given. No sender was found in the message. No such command: $command_name No such list found: $spec No such list matching spec: $listspec No such list: $_list No such list: $listspec No such queue: $queue Not a Mailman 2.1 configuration file: $pickle_file Nothing to do Notify list owners/moderators of pending requests. Number of objects found (see the variable 'm'): $count Operate on digests. Ordinary discussion mailing list style. Original Message PID unreadable in: $config.PID_FILE Perform ARC auth checks and attach resulting headers Perform auth checks and attach Authentication-Results header. Perform some bookkeeping after a successful post. Poll the NNTP server for messages to be gatewayed to mailing lists. Post to a moderated newsgroup gateway Posting of your message titled "$subject" Print detailed instructions and exit. Print less output. Print some additional status. Print the Mailman configuration. Process DMARC reject or discard mitigations Produces a list of member names and email addresses.

The optional delivery= and mode= arguments can be used to limit the report
to those members with matching delivery status and/or delivery mode.  If
either delivery= or mode= is specified more than once, only the last occurrence
is used.
 Programmatically, you can write a function to operate on a mailing list, and
this script will take care of the housekeeping (see below for examples).  In
that case, the general usage syntax is:

% mailman withlist [options] -l listspec [args ...]

where `listspec` is either the posting address of the mailing list
(e.g. ant@example.com), or the List-ID (e.g. ant.example.com). Reason: {}

 Regenerate the aliases appropriate for your MTA. Regular expression requires --run Reject/bounce a message and stop processing. Remove DomainKeys headers. Remove a mailing list. Removed list: $listspec Reopening the Mailman runners Request to mailing list "$display_name" rejected Restarting the Mailman runners Send an acknowledgment of a posting. Send automatic responses. Send the message to the outgoing queue. Sender: {}
 Show a list of all available queue names and exit. Show also the list descriptions Show also the list names Show the current running status of the Mailman system. Show this help message and exit. Shutting down Mailman's master runner Signal the Mailman processes to re-open their log files. Stale pid file removed. Start the Mailman master and runner processes. Starting Mailman's master runner Stop and restart the Mailman runner subprocesses. Stop processing commands. Stop the Mailman master and runner processes. Subject: {}
 Subscription request Suppress some duplicates of the same message. Suppress status messages Tag messages with topic matches. The Mailman Replybot The attached message matched the $mlist.display_name mailing list's content
filtering rules and was prevented from being forwarded on to the list
membership.  You are receiving the only remaining copy of the discarded
message.

 The built-in -owner posting chain. The built-in header matching chain The built-in moderation chain. The built-in owner pipeline. The built-in posting pipeline. The mailing list is in emergency hold and this message was not
        pre-approved by the list administrator.
         The master lock could not be acquired because it appears as though another
master is already running. The master lock could not be acquired, because it appears as if some process
on some other host may have acquired it.  We can't test for stale locks across
host boundaries, so you'll have to clean this up manually.

Lock file: $config.LOCK_FILE
Lock host: $hostname

Exiting. The master lock could not be acquired.  It appears as though there is a stale
master lock.  Try re-running $program with the --force flag. The message comes from a moderated member The message has a matching Approve or Approved header. The message has no valid senders The message is larger than the {} KB maximum size The message is not from a list member The message's content type was explicitly disallowed The message's content type was not explicitly allowed The message's file extension was explicitly disallowed The message's file extension was not explicitly allowed The results of your email command are provided below.
 The results of your email commands The sender is in the nonmember {} list The variable 'm' is the $listspec mailing list The virgin queue pipeline. The {} list has {} moderation requests waiting. The {} list has {} moderation requests waiting.

{}
Please attend to this at your earliest convenience.
 There are two ways to use this script: interactively or programmatically.
Using it interactively allows you to play with, examine and modify a mailing
list from Python's interactive interpreter.  When running interactively, the
variable 'm' will be available in the global namespace.  It will reference the
mailing list object. This script provides you with a general framework for interacting with a
mailing list. Today's Topics ($count messages) Today's Topics:
 Uncaught bounce notification Undefined domain: $domain Undefined runner name: $name Unrecognized or invalid argument(s):
{} Unshunt messages. Unsubscription request User: {}
 Welcome to the "$mlist.display_name" mailing list${digmode} You are not allowed to post to this mailing list From: a domain which publishes a DMARC policy of reject or quarantine, and your message has been automatically rejected.  If you think that your messages are being rejected in error, contact the mailing list owner at ${listowner}. You are not authorized to see the membership list. You can print the list's posting address by running the following from the
command line:

% mailman withlist -r listaddr -l ant@example.com
Importing listaddr ...
Running listaddr.listaddr() ...
ant@example.com You have been invited to join the $event.mlist.fqdn_listname mailing list. You have been unsubscribed from the $mlist.display_name mailing list You will be asked to confirm your subscription request and you may be issued a
provisional password.

By using the 'digest' option, you can specify whether you want digest delivery
or not.  If not specified, the mailing list's default delivery mode will be
used.  You can use the 'address' option to request subscription of an address
other than the sender of the command.
 Your confirmation is needed to join the $event.mlist.fqdn_listname mailing list. Your confirmation is needed to leave the $event.mlist.fqdn_listname mailing list. Your message to $mlist.fqdn_listname awaits moderator approval Your new mailing list: $fqdn_listname Your subscription for ${mlist.display_name} mailing list has been disabled Your urgent message to the $mlist.display_name mailing list was not authorized
for delivery.  The original message as received by Mailman is attached.
 [$mlist.display_name]  [----- end pickle -----] [----- start pickle -----] [ADD] %s [DEL] %s [No bounce details are available] [No details are available] [No reason given] [No reasons given] bad argument: $argument domain:admin:notice:new-list.txt help.txt ipython is not available, set use_ipython to no list:admin:action:post.txt list:admin:action:subscribe.txt list:admin:action:unsubscribe.txt list:admin:notice:disable.txt list:admin:notice:removal.txt list:admin:notice:subscribe.txt list:admin:notice:unrecognized.txt list:admin:notice:unsubscribe.txt list:member:digest:header.txt list:member:digest:masthead.txt list:member:generic:footer.txt list:member:regular:header.txt list:user:action:invite.txt list:user:action:subscribe.txt list:user:action:unsubscribe.txt list:user:notice:goodbye.txt list:user:notice:hold.txt list:user:notice:no-more-today.txt list:user:notice:post.txt list:user:notice:probe.txt list:user:notice:refuse.txt list:user:notice:rejected.txt list:user:notice:warning.txt list:user:notice:welcome.txt n/a readline not available slice and range must be integers: $value Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-10-26 04:46+0000
Last-Translator: Victoriano Giralt <victoriano@uma.es>
Language-Team: Spanish <https://hosted.weblate.org/projects/gnu-mailman/mailman/es/>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.3.2-dev
 
    Ejecutar un script.  El argumento es la ruta de acceso del módulo a algo invocable.  Éste
    se importará y luego, si también se da --listspec/-l, se
    llama con la lista de correo como primer argumento.  Si se dan
    argumentos adicionales al final de la línea de comandos, se pasan como
    argumentos posicionales posteriores a lo invocable.  Para más ayuda, consultar
    --details.

Si no se proporciona ningún argumento --listspec/-l, la función de script se llama
    sin argumentos.
     
    Inicia el corredor indicado, que debe ser una de las cadenas devueltas por la
    opción -l.

    Para los corredores que administran un directorio de cola, se indica un `slice:range` opcional
    que se usa para asignar varios procesos corredores a esa cola.  El rango es el
    número total de corredores para la cola, mientras que la sección es el número de este
    corredor de [0..rango).  Para los corredores que no administran una cola, la sección y
    rango se ignoran.

    Al usar el formulario `slice:range`, debe asegurarse de que cada corredor para la
    cola recibe el mismo valor de rango.  Si no se indica `slice:runner`, entonces
    se usa 1:1.
     
- Hecho. 
- Ignorados: 
- Resultados: 
- Sin procesar: 
Mensajes retenidos:
 
Suscripciones retenidas:
 
Dessuscripciones retenidas:
     Una salida más detallada que incluye las rutas del sistema de archivos que Mailman está
    usando.     Una especificación de la lista de correo sobre la que operar.  Esta puede ser la dirección
    de envío de la lista, o su List-ID.  El argumento también puede ser una expresión regular
    de Python, en cuyo caso se compara tanto con la dirección
    de envío y List-ID de todas las listas de correo.  Para usar una expresión regular,
    LISTSPEC debe comenzar con ^ (y la coincidencia se realiza con re.match().
    LISTSPEC no puede ser una expresión regular a menos que se ponga --run.     Añadir todas las direcciones incluidas en FILENAME con el modo de entrega especificado
    con -d/--delivery.  FILENAME puede ser '-' para indicar al entrada estándar.
    Se ignoran las líneas vacías y aquellas que comienzan con '#'.
         Añadir y borrar suscriptores según se necesario para sincronizar las suscripciones
    de una lista con un archivo de entrada. FILENAME es el archivo que contiene el
    nuevo listado. Puede ser '-' para indicar la entrada estándar. Se ignorarán
    las líneas en blanco y las que comiencen por '#'. Las direcciones contenidas
    en FILENAME que no estén suscritas se añadirán a la lista con el modo de entrega
    indicado con -d/--delivery. Los suscriptores cuyas direcciones no estén incluidas
    en FILENAME se eliminarán de la lista.
         Parejas clave/valor de metadatos adicionales para agregar a los metadatos del diccionario
    del mensaje.  Usar el formato clave=valor.  Se permiten varias opciones -m.     Archivo de configuración que se va a usar.  Si no se proporciona, la variable de entorno
    MAILMAN_CONFIG_FILE se consulta y se usa si está definida.  Si ninguno de los dos se proporciona,
    se carga el archivo de configuración predeterminado.     Crear una lista de correo.

    El 'nombre de lista completo', es decir, la dirección de envío a la lista
    es necesario.  Debe ser una dirección de correo electrónico válida y el dominio debe estar
    registrado en Mailman.  Los nombres de lista han de ser en minúsculas.     Fecha: $date     Eliminar todos los suscriptores de la lista. Si se especifica, no se
    podrán usar -f/--file, -m/--member ni --fromall.
         Eliminar suscriptores cuyas direcciones aparecen en FILENAME además de
    aquellas que se pudieran especificar con -m/--member. FILENAME puede
    ser '-' para indicar la entrada estándar. Se ignorarán las líneas en blanco y
    las que empiecen con '#'.
         Eliminar miembros de una lista de correo.     Eliminar el suscriptor cuya dirección es ADDRESS además de aquellos
    especificados con -f/--any si lo hay. Esta opción se puede repetir para
    múltiples direcciones.
         Eliminar el(los) suscriptor(es) indicado por -m/--member y/o -f/--file de
    todas las listas de la instalación. Esta opción no se puede especificar con
    -a/--all o -l/--list.
         Descartar todos los mensajes desviados en lugar de moverlos de vuelta a su original
    Cola.     Mostrar, añadir o eliminar los miembros de una lista de correo.
    El filtrado a lo largo de varios criterios se puede hacer al mostrar.
    Sin opciones dadas, mostrar a los miembros de la lista de correo
    en stdout es el modo predeterminado.
         En realidad no hacer nada, pero combinado con --verbose, mostrar lo que
    sucedería.     No hacer cambios. En lugar de ellos, se muestra los que se haría
   con la lista.     No intentar mostrar con formato el objeto.  Esto es útil si hay algunos
    problemas con el objeto y sólo se quiere obtener una representación
    decodificada.  Es útil con 'mailman qfile -i <file>'.  En ese caso, la
    lista de objetos decodificados se dejará en una variable llamada 'm'.     No mostrar mensajes de estado.  Los mensajes de error se siguen mostrando en la salida estándar
    de errores.     No reiniciar los corredores cuando salgan debido a un error o a un SIGUSR1.
    Utilícelo solo para depuración.     Archivo al que enviar la salida.  Si no se da, o si se da '-', se
    usa la salida estándar.     Archivo al que enviar la salida.  Si no se indica, se usa la salida estándar.     De: $from_     Generar el archivo de alias al arrancar. Algunos MTA, como Postfix,
    no pueden entregar correo si el archivo de alias indicado en su
    configuración no está disponible. En algunas circunstancias, esto
    podría generar un bloqueo la primera vez que se arranque el 
    servidor de mailman3. Poner esta opción a "true", hará que 
    este proceso cree los archivos y así permitir al MTA fucnionar
    sin problemas.     Si el observador principal encuentra un bloqueo principal existente, normalmente saldrá
    con un mensaje de error.  Con esta opción, el principal realizará un
    nivel adicional de comprobación.  Si un proceso que coincide con el host/pid descrito en el
    archivo de bloqueo se está ejecutando, el principal todavía se cerrará, lo que requiere que manualmente
    elimine el bloqueo.  Pero si no se encuentra ningún proceso de coincidencia, el principal
    eliminará el bloqueo aparentemente obsoleto y hará otro intento de reclamar el
    bloqueo principal.     Si el observador principal encuentra un bloqueo principal existente, normalmente saldrá
    con un mensaje de error.  Con esta opción, el principal realizará un
    nivel adicional de comprobación.  Si un proceso que coincide con el host/pid descrito en el
    archivo de bloqueo se está ejecutando, el principal todavía se cerrará, lo que requiere que manualmente
    elimine el bloqueo.  Pero si no se encuentra ningún proceso de coincidencia, el principal
    eliminará el bloqueo aparentemente obsoleto y hará otro intento de reclamar el
    bloqueo principal.     Importar datos de listas de Mailman 2.1. Requiere el nombre completo
    (fully-qualified) de la lista que se importa y la ubicación del fichero pickle
    de Mailman 2.1.     Incrementar el número de volumen de resumen y restablecer el número de resumen a uno.  Si
    dado con --send, el número de volumen se incrementa antes de que cualquier
    resumen se envíe.     Clave que se usará para la búsqueda.  Si no se da ninguna sección, todos las parejas clave-valor
    de cualquier sección que coincida con la clave dada se mostrarán.     Le deja en un cursor interactivo una vez se haya completado el resto de procesamiento.
    Este es el valor predeterminado a menos que se dé la opción --run.     Enumerar sólo las listas de correo alojadas en el dominio dado, que
    debe ser el nombre de host de correo.  Se pueden dar varias opciones -d.
         Vigilante principal de subprocesos.

    Comience y observe a los corredores configurados, asegurándose de que permanezcan con vida y
    activos.  Cada corredor se bifurca y exec'd a su vez, con el principal esperando
    en sus identificadores de proceso.  Cuando detecta que un corredor hijo ha salido, se puede
    reiniciar.

    Los corredores responden a SIGINT, SIGTERM, SIGUSR1 y SIGHUP.  SIGINT,
    SIGTERM y SIGUSR1 hacen que un corredor salga limpiamente.  El principal
    reiniciará los corredores que han salido debido a un SIGUSR1 o algún tipo de otro
    condición de salida (por ejemplo, debido a una excepción no capturada).  SIGHUP causa que el
    principal y los corredores cierren sus archivos de registro, y los reabran a continuación, en el
    siguiente mensaje impreso.

    El principal también responde a SIGINT, SIGTERM, SIGUSR1 y SIGHUP, que
    simplemente lo pasa a los corredores.  Tenga en cuenta que el principal cerrará y
    reabrirá sus propios archivos de registro al recibir un SIGHUP.  El principal también deja
    su propio identificador de proceso en el archivo especificado en el archivo de configuración, pero
    normalmente no es necesario usar este PID directamente.     ID de mensaje: $message_id     Nombre del archivo que contiene el mensaje a insertar.  Si no se da, o
    '-' (sin comillas) se usa la entrada estándar.     Normalmente, este script se negará a correr si el identificador de usuario y el identificador de grupo
    no son el usuario y grupo 'mailman' (como se define cuando configuró
    Mailman).  Si se ejecuta como root, este script cambiará a este usuario y grupo
    antes de que se haga la comprobación.

    Esto puede ser un inconveniente para las prueba y depuración, por lo que la opción -u
    significa que se salta el paso que establece y comprueba el uid/gid, y el
    programa se ejecuta como el usuario y el grupo actuales.  Esta opción no se recomienda
    para entornos de producción normales.

    Tenga en cuenta, sin embargo, que si se ejecuta con -u y no está en el grupo de mailman,
    puede tener problemas de permisos, como no poder eliminar los archivos
    de una lista a través de la web.  ¡Suerte complicada!     Notificar al propietario de la lista por correo que su lista de correo se ha
    creado.     Operar en una lista de correo.

    Para ayuda detallada, ver --details
         Operar en esta lista de correo.  Se pueden dar varias opciones --list.  El
    argumento puede ser un List-ID o un nombre de lista completo.  Sin
    esta opción, opera en los resúmenes de todas las listas de correo.     Operar en esta lista de correo.  Se pueden dar varias opciones --list.  El
    argumento puede ser un List-ID o un nombre de lista completo.  Sin
    esta opción, operar en las solicitudes de todas las listas de correo.     Invalida el conjunto predeterminado de corredores que invocará el principal, que es
    normalmente definido en el archivo de configuración.  Múltiples opciones -r se pueden
    proporcionar.  Los valores de -r se pasan directamente a bin/runner.     Sustituir el parámetro de la lista para admin_notify_mchanges.     Sustituir la opción de la lista para send_goodbye_message
    a los suscriptores eliminados.     Sustituir el parámetro de la lista para send_welcome_message a los miembros suscritos.     Sustituir la configuración de la lista para send_welcome_message.     Registrar el dominio de la lista de correo si aún no está registrado.  Este es
    el comportamiento predeterminado, pero estas opciones se proporcionan por
    compatibilidad con el pasado.  Con -D no registrar el dominio de la lista de correo.     Ejecuta el corredor indicado exactamente una vez a través de su bucle principal.  De lo contrario, el
    corredor se ejecuta indefinidamente hasta que el proceso recibe una señal.  Esto no es
    compatible con corredores que no se pueden ejecutar una sola vez.     Sección que se usará para la búsqueda.  Si no se da ninguna clave, todos los pares clave-valor
    de la sección dada se mostrarán.     Enviar los resúmenes recopilados para la Lista solo si su digest_send_periodic
    se pone a Verdadero.     Enviar cualesquiera resúmenes recogidos hasta el momento, incluso si el umbral de tamaño no
    se ha alcanzado.     Enviar una invitación a los miembros que se añaden en lugar de incluirlos inmediatamente.
         Definir el modo de entrega de los suscriptores que se añaden como
    'regular', 'mime', 'plain', 'summary' or 'disabled'. I.e., una entre regular,
    los tres modos de resumen o sin entrega. Por defecto es regular.     Pone el modo de entrega de los miembros que se añaden a 'regular', 'mime', 'simple',
    'resumen' o 'desactivado'.  I.e., uno de los siguientes: regular, tres modos de resumen
    o sin entregas. Si no se indica, la opción por defecto es regular. Se ignora para los
    miembros invitados.     Establecer el idioma preferido de la lista a CÓDIGO, que debe ser un
    código de idioma de dos letras.     Especificar una dirección de correo del propietario de la lista.  Si la dirección no está actualmente
    registrada en Mailman, la dirección se registra y vincula a un usuario.
    Mailman enviará un mensaje de confirmación a la dirección, pero también
    enviará un aviso de creación de lista a la dirección.  Se puede especificar más de un
    propietario.     Especificar la codificación de las cadenas contenidas en PICKLE_FILE
    si no es utf-8 o un subconjunto del mismo. Normalmente será el juego
    de caracteres de Mailman 2.1 de la opción preferred_languaje de la lista.     Empieza un corredor.

    Se inicia el corredor nombrado en la línea de comandos, y puede ejecutar
    a través de su bucle principal una vez (para aquellos corredores que soporten esto) o
    continuamente.  Esto último es cómo el corredor principal comienza todos sus
    subprocesos.

    -r es necesario a menos que se dé -l o -h, y su argumento debe ser uno de los
    los nombres mostrados por el modificador -l.

    Normalmente, este script debe iniciarse desde `mailman start`.  Ejecutarlo
    por separado o con -o es generalmente útil sólo para depuración.  Cuando se ejecuta
    de esta manera, la variable de entorno $MAILMAN_UNDER_MASTER_CONTROL será
    definida lo que cambia sutilmente algún comportamiento de control de errores.
         Iniciar una sesión interactiva de Python, con una variable llamada 'm'
    que contenga la lista de objetos no decodificados.     Asunto: $subject     La lista sobre la que se va a trabajar. Se requiere a menos que se especifique --fromall.
         El nombre de la cola a la que se inyectará el mensaje.  QUEUE debe ser uno de los
    directorios dentro del directorio qfiles.  Si se omite, se utiliza
    la cola entrante.     [MODE] Añadir todas las direcciones de suscripción contenidas en
    FILENAME. Puede ser '-' para indicar la entrada estándar. Se ignorarán
    las líneas en blanco y las que comiencen por '#'. Esta opción se ha
    sustituido por 'mailman addmembers'.     [MODE] Eliminar todas las direcciones de suscriptores contenidas en
    FILENAME de la lista indicada. Puede ser '-' para indicar la entrada
    estándar. Se ignorarán las líneas en blanco y las que comiencen por '#'.
    Esta opción se ha sustituido por 'mailman delmembers'.     [MODO] Mostrar la salida a FILENAME en lugar de stdout.  FILENAME
    puede ser '-' para indicar la salida estándar.     [MODE] Sincronizartodas las direcciones de suscripción de la lista indicada
    con las contenidas en FILENAME. Puede ser '-' para indicar la entrada
    estándar. Se ignorarán las líneas en blanco y las que comiencen por '#'.
    Esta opción se ha sustituido por 'mailman syncmembers'.     [filtro de salida] Mostrar solo miembros de resumen de tipo.
    «cualquiera» significa cualquier tipo de resumen, «texto sin formato» significa texto sin formato (rfc 1153)
    tipo digests, 'mime' significa resúmenes de tipo MIME.     [filtro de salida] Mostrar solo miembros con un ROL determinado.
    El rol puede ser ‘any’ (cualquiera), 'member’, (miembro) 'nonmember’ (nomiembro), ‘owner’ (propietario), 'moderador',
    o 'administrator' (administrador) (por ejemplo, propietarios y moderadores).
    Si no se proporciona, se usan los miembros de la entrega.      [filtro de salida] Mostrar solo miembros con un estado de entrega determinado.
    'habilitado' se refiera a todos los miembros cuya entrega esté habilitada, 'any' se refiere
    a miembros cuya entrega está inhabilitada por cualquier razón, 'byuser' se refiere
    a que el miembro desactivó su propia entrega, 'bybounces' se refiere a que
    la entrega se desactivo por el procesador automático de rebotes,
    'byadmin' se refiere a que la entrega fue desactivada por la lista
    administrador o moderador, y 'unknown' se refiere a que la entrega se deshabilitó
    por razones desconocidas (antiguas).     [filtro de salida] Mostrar solo los miembros de entrega normales.  (Modo Resumen) Listas de correo coincidentes con $count encontradas: $display_name confirmación de envío Se le ha dado de baja de la lista de correo ${mlist.display_name} debido a rechazos de mensajes La suscripción de $member a $mlist.display_name está desactivada $mlist.display_name Resumen, Vol $volume, Número $digest_number $mlist.display_name mensaje de prueba de lista de correo Notificación de suscripción a $mlist.display_name notificación de baja de $mlist.display_name El envío a $mlist.fqdn_listname de $msg.sender precisa de aprobación $mlist.list_id saltado al volumen $mlist.volume, número ${mlist.next_digest_number} $mlist.list_id no tiene miembros $mlist.list_id se encuentra en el volumen $mlist.volume, número ${mlist.next_digest_number} $mlist.list_id enviado volumen $mlist.volume, número ${mlist.next_digest_number} $name corre $classname $person tiene una suscripción pendiente para $listname $person dejó $mlist.fqdn_listname $realname a través de $mlist.display_name $self.name: $email no es miembro de $mlist.fqdn_listname $self.name: No se encuentra ninguna dirección válida para suscribirse $self.name: No se encuentra una dirección de correo válida para darse de baja $self.name: no hay tal comando: $command_name $self.name: demasiados argumentos: $printable_arguments (sin asunto) - Detalles originales del mensaje: Las opciones --send y --periodic no se pueden usar a la vez <----- inicio de objeto $count -----> Una ejecución anterior de GNU Mailman no salió limpiamente ({}).  Pruebe usar --force Una regla que siempre encaja. Aceptar un mensaje. Agregue un prefijo específico de la lista al valor de la cabecera Asunto. Agregue las cabeceras RFC 2369 List-*. Agregue el mensaje a los archivos. Agregue el mensaje al resumen, posiblemente enviándolo. Después de filtrar el contenido, el mensaje estaba vacío Ya suscrito (omitiendo): $display_name <$email> Ya suscrito (omitiendo): $email Un alias para 'fin'. Un sinónimo de 'unirse'. Un sinónimo de 'dejar'. Un directorio alternativo para dejar los diversos archivos MTA. Y puede mostrar la dirección de solicitud de la lista ejecutando:

% mailman withlist -r listaddr.requestaddr -l ant@example.com
Importando listaddr ...
Corriendo listaddr.requestaddr() ...
ant-request@example.com Anunciar solo el estilo de la lista de correo. Aplicar mitigaciones DMARC. Como otro ejemplo, supongamos que desea cambiar el nombre a mostrar de una determinada
lista de correo.  Puede colocar la siguiente función en un archivo llamado
`change.py`:

cambio def (mlist, display_name):
    mlist.display_name = display_name

y ejecutar esto desde la línea de comandos:

% mailman withlist -r change -l ant@example.com 'My List'

Tenga en cuenta que no hay que confirmar explícitamente ninguna transacción de la base de datos, ya que
Mailman hará esto por usted (suponiendo que no se hayan producido errores). Respuesta automática para su mensaje a la lista de correo "$display_name" Especificación de corredor incorrecta: $value Calcular los destinatarios del propietario y del moderador. Calcular los destinatarios normales del mensaje. No se puede importar el módulo corredor: $class_path No se puede procesar como dirección de correo válida (omitiendo): $line No se puede des-desviar el mensaje $filebase, omitiendo:
$error Capturar mensajes que son mayores que un máximo especificado. Capturar mensajes con destino implícito. Capturar mensajes sin cabecera Asunto o vacía. Captura mensajes con cabeceras sospechosas. Capturar mensajes con demasiados destinatarios explícitos. Capturar comandos de correo mal dirigidos. Limpiar ciertas cabeceras de todos los mensajes. Confirmar la solicitud de suscripción. Correo de confirmación enviado a $person Correo de confirmación enviado a $person para salir de $mlist.fqdn_listname Las claves de confirmación no coinciden Confirmado Notificación de mensaje de filtro de contenido Lista de correo creada: $mlist.fqdn_listname Moderación DMARC Decorar un mensaje con cabeceras y pies de página. Pié de página del resumen Cabecera del resumen (digest) Desecha un mensaje y detiene el procesamiento. Estilo de lista de correo de discusión con archivos privados. Mostrar la versión de Mailman. Mostrar más información de depuración en el archivo de registro. Devuelve tus argumentos. La moderación de emergencia está en vigor para esta lista Fin de  Filtrar el contenido MIME de los mensajes. Busque la directiva DMARC de From: dominio. Por razones desconocidas, no se pudo adquirir el bloqueo principal.

Archivo de bloqueo: $config. LOCK_FILE
Host de bloqueo: $hostname

Saliendo. Reenvío de mensaje moderado GNU Mailman ya se está ejecutando GNU Mailman está en un estado inesperado ($hostname != $fqdn_name) GNU Mailman no se está ejecutando GNU Mailman se está ejecutando (pid del principal: $pid) GNU Mailman se ha parado (pid obsoleto: $pid) Generando los mapas de alias del MTA Obtener un listado de suscriptores de la lista. Obtener ayuda sobre los comandos de correo disponibles. Extraer información de un archivo de cola. Obtener los destinatarios de entrega normales de un archivo de inclusión. La cabecera “{}” encajó con una línea de bounce_matching_header La cabecera "{}" encaja con una regla de cabecera Este es un ejemplo de cómo usar la opción --run.  Digamos que tiene un archivo en el
directorio de instalación de Mailman llamado 'listaddr.py', con las dos siguientes
funciones:

def listaddr(mlist):
    print(mlist.posting_address)

def requestaddr(mlist):
    print(mlist.request_address)

Los métodos de ejecución toman al menos un argumento, el objeto de lista de correo para operar sobre ella.
Los argumentos adicionales en en la línea de comandos se pasan como
argumentos posicionales a los llamables.

Si no se proporciona -l, puede ejecutar una función que no toma ningún argumento.
 Retiene un mensaje y detiene procesamiento. Si responde a este mensaje, manteniendo la cabecera Subject: (Asunto) intacta,
Mailman descartará el mensaje retenido.  Haga esto si el mensaje es spam.
Si responde a este mensaje incluyendo una cabecera Approved: con la contraseña
de la lista en ella, se aprobará el mensaje para que se entregue a la lista.
La cabecera Approved: puede aparecer también en la primera línea
del cuerpo de la respuesta. Ignorando no diccionario: {0!r} Ignorando partes MIME que no son texto sin formato Nombre de lista ilegal: $fqdn_listname Direcciones ilegales del propietario: $invalid Información sobre esta instancia de Mailman. Inyectar un mensaje de un archivo en la cola de una lista de correo. Código de idioma no válido: $language_code Dirección de correo no válida o no verificada: $email Valor no válido para [shell]use_python: {} ¿Está el principal corriendo? Unirse a esta lista de correo. Última notificación de autorespuesta de hoy Dejar esta lista de correo. Dejar esta lista de correo.

Se le puede pedir confirmar su solicitud. Menos detalle Listar todas las listas de correo. La lista ya existe: $fqdn_listname Listar solo las listas de correo que se anuncian públicamente Enumera los nombres de los corredores disponibles y sale. Busque un bucle de publicación. Buscar cualquier activación de regla previo. Seleccionar todos los mensajes publicados en una lista de correo
        interconectado con un grupo de noticias moderado.
         Seleccionar mensajes enviados por direcciones vetadas. Seleccionar mensajes enviados por miembros moderados. Seleccionar mensajes enviados por no miembros. Seleccionar mensajes sin remitentes válidos. Miembro no suscrito (omitiendo): $display_name <$email> Miembro no suscrito (omitiendo): $email Suscriptores de la lista {}:
{} La dirección $email no tiene permitido suscribirse (omitiendo) El mensaje contiene administrivia El mensaje ya se ha publicado en esta lista El mensaje no va dirigido explícitamente a la lista El mensaje tiene más de {} destinatarios El mensaje no tiene asunto El remitente del mensaje {} está vetado en esta lista Cadena de moderación Modificar cabeceras de mensajes. Mover el mensaje a la cola de noticias saliente. N/D Nueva solicitud de suscripción a $self.mlist.display_name desde $self.address.email Nueva solicitud de baja de $mlist.display_name por $email Nueva solicitud de baja a $self.mlist.display_name desde $self.address.email Ningún hijo con pid: $pid No se ha encontrado ningún token de confirmación No se ha encontrado ninguna lista de correo que coincida No hay usuario registrado para la dirección de correo: $email No se ha proporcionado ningún nombre de corredor. No se encontró ningún remitente en el mensaje. No existe tal comando: $command_name No se encontró la lista: $spec No se encontró esa espec de listas coincidentes: $listspec No hay tal lista: $_list No hay tal lista: $listspec No hay tal cola: $queue No es un archivo de configuración de Mailman 2.1: $pickle_file Nada que hacer Notificar a los propietarios/moderadores de la lista sobre solicitudes pendientes. Número de objetos encontrados (ver la variable 'm'): $count Operar en resúmenes. Estilo de lista de correo de discusión ordinaria. Mensaje original PID ilegible en: $config. PID_FILE Realice comprobaciones de autentificación ARC y adjuntar las cabeceras resultantes Realice las comprobaciones de autentificación y adjunte la cabecera Authentication-Results. Realizar alguna contabilización tras un envío exitoso. Consultar el servidor NNTP para transferir mensajes a listas de correo. Publicar en una pasarela de grupo de noticias moderado Publicación de su mensaje titulado "$subject" Mostrar instrucciones detalladas y salir. Mostrar menos salida. Mostrar algún estado adicional. Mostrar la configuración de Mailman. Procesar mitigaciones de rechazo o descarte de DMARC Genera una lista de los nombres y direcciones de los suscriptores.

Se pueden usar los argumentos opcionales digest= y mode= para limitar el listado
a aquellos suscriptores cuyo modo de entrega y/o estado coincida con el indicado.
Si se indica bien delivery= o mode= más de una vez, solo se tendrá en cuenta la
última.
 Por programa, puede escribir una función para operar en una lista de correo, y
este script se encargará de la limpieza (ver más abajo para ver ejemplos).  En
en ese caso, la sintaxis de uso general es:

% mailman withlist [options] -l listspec [args ...]

donde `listspec` es la dirección de envío de la lista de correo
(por ejemplo, ant@example.com) o la List-ID (por ejemplo, ant.example.com). Motivo: {}

 Volver a generar los alias adecuados para su MTA. La expresión regular requiere --run Rechazar/rebotar un mensaje y detener el procesamiento. Quitar las cabeceras DomainKeys. Eliminar una lista de correo. Lista eliminada: $listspec Reabriendo los corredores de Mailman Solicitud a lista de correo "$display_name" rechazada Reiniciando los corredores de Mailman Enviar una confirmación de un envío. Enviar respuestas automáticas. Envíe el mensaje a la cola de salida. Remitente: {}
 Mostrar una lista de todos los nombres de cola disponibles y salir. Mostrar también las descripciones de listas Mostrar también los nombres de listas Mostrar el estado actual de ejecución del sistema Mailman. Muestrar este mensaje de ayuda y salir. Deteniendo el corredor principal de Mailman Enviar señal a los procesos de Mailman para volver a abrir sus archivos de registro. Fichero pid obsoleto borrado. Iniciar los procesos principal y corredor de Mailman. Iniciar el corredor principal de Mailman Detener y reiniciar los subprocesos corredores de Mailman. Detener el procesamiento de comandos. Detenga los procesos principal y corredor de Mailman. Asunto: {}
 Solicitud de suscripción Suprima algunos duplicados del mismo mensaje. Suprimir mensajes de estado Etiquetar mensajes con coincidencias de temas. El contestador automático de Mailman El mensaje adjunto encajó con las reglas de filtrado de contenido de la
lista de correo $mlist.display_name y se evitó que se reenviara a los
miembros de la lista.  Está recibiendo la única copia restante del
mensaje.

 La cadena de moderación incorporada de -owner. La cadena de coincidencia de encabezado incorporada La cadena de moderación incorporada. El canal integrado del propietario. El canal integrado de envío. La lista de correo está en espera de emergencia y este mensaje no fue
        aprobado previamente por el administrador de la lista.
         El bloqueo principal no se pudo adquirir porque parece que otro
principal ya se está ejecutando. El bloqueo principal no se pudo adquirir, porque parece como si algún proceso
en algún otro host puede haberlo adquirido.  No podemos probar si hay cerraduras obsoletas más allá de
los límites del host, por lo que tendrá que limpiar esto manualmente.

Archivo de bloqueo: $config. LOCK_FILE
Host de bloqueo: $hostname

Saliendo. No se pudo adquirir el bloqueo principal.  Parece como si hubiera un
bloqueo principal obsoleto.  Intente volver a ejecutar $program con la marca --force. El mensaje proviene de un miembro moderado El mensaje tiene una cabecera Aprobar o Aprobado que encaja. El mensaje no tiene remitentes válidos El mensaje es mayor que el tamaño máximo de {} KB El mensaje no es de un miembro de la lista El tipo de contenido del mensaje ha sido inhabilitado explícitamente El tipo de contenido del mensaje no se ha permitido explícitamente La extensión de fichero del mensaje se ha inhabilitado explícitamente La extensión de fichero del mensaje no se ha permitido explícitamente Los resultados de su comando de correo se proporcionan a continuación.
 Resultados de los comandos enviados por correo El remitente está en la lista {} de no miembros La variable 'm' es la $listspec de la lista de correo El canal de la cola virgen. La lista {} tiene {} peticiones de moderación esperando. La lista {} tiene {} peticiones de moderación esperando.

{}
Por favor, atienda esto cuanto antes.
 Hay dos formas de usar este script: de forma interactiva o por programa.
El uso interactivo permite jugar, examinar y modificar una lista de
correo desde el intérprete interactivo de Python.  Cuando se ejecuta de forma interactiva,
la variable 'm' estará disponible en el espacio de nombres global.  Hará referencia al
objeto lista de correo. Este script le proporciona un marco general para interactuar con una
lista de correo. Temas de hoy ($count mensajes) Temas del día:
 Notificación de rebote no captado Dominio indefinido: $domain Nombre del corredor no definido: $name Argumento(s) no reconocido o incorrecto:
{} Mensajes des-desviados. Solicitud de baja Usuario: {}
 Bienvenido a la lista de correo "$mlist.display_name"${digmode} No se le permite publicar en esta lista de correo Desde: un dominio que publica una política DMARC de rechazo o cuarentena, y su mensaje se ha rechazado automáticamente.  Si cree que sus mensajes se rechazan por error, póngase en contacto con el propietario de la lista de correo en ${listowner}. No está usted autorizado a ver el listado de miembros. Puede mostrar la dirección de envío de la lista ejecutando lo siguiente desde la
línea de comandos:

% mailman withlist -r listaddr -l ant@example.com
Importando listaddr ...
Corriendo listaddr.listaddr() ...
ant@example.com Le han invitado a participar en la lista de correo $event.mlist.fqdn_listname. Se le ha dado de baja de la lista de correo $mlist.display_name Se le pedirá que confirme su petición de suscripción y puede que se le asigne
una clave provisional.

Usando la opción 'digest', puede especificar si quiere recibir los envíos como
un resumen o no. Si no lo indica, se usará el modo de envío que tenga definido
la lista. Puede usar la opción 'address' para solicitar que se suscriba una dirección
distinta a la que está enviando el comando.
 Se requiere confirmación de su intención de unirse a la lista de correo $event.mlist.fqdn_listname. Es necesario confirmar que quiere abandonar la lista de correo $event.mlist.fqdn_listname. Su mensaje a $mlist.fqdn_listname espera la aprobación del moderador Su nueva lista de correo: $fqdn_listname Se ha desactivado su suscripción a la lista de correo ${mlist.display_name} Su mensaje urgente a la lista de correo $mlist.display_name no fue autorizado
para la entrega.  Se adjunta el mensaje original recibido por Mailman.
 [$mlist.display_name]  [----- fin pickle -----] [----- inicio pickle -----] [AÑADIR] %s [BORRAR] %s [No hay detalles de rebotes disponibles] [No hay detalles disponibles] [No se ha dado ninguna razón] [No hay razones dadas] argumento incorrecto: $argument La lista de correo '$listname' acaba de ser creada para usted.
A continuación se ofrece información básica sobre su lista de correo.

Hay una interfaz basada en el correo para usuarios (no administradores) de su lista;
puede obtener información sobre su uso enviando un mensaje con sólo la palabra 'help'
como asunto o en el cuerpo, a:

    $request_email

Por favor, dirija todas las preguntas a $site_email. Ayuda para la lista de correo $listname

Este es el comando de correo 'help' para la versión $version del gestor de listas de correo GNU Mailman en $domain.

A continuación se describen los comandos que puedes enviar para obtener información y controlar tu suscripción a las listas de correo de este sitio. Un comando puede estar en la línea de Asunto o en el cuerpo del mensaje.

Los comandos deben enviarse a la dirección ${listname}-request@${domain}. 

Acerca de las descripciones - las palabras entre "<>" significan elementos REQUERIDOS y las palabras en "[]" denotan elementos OPCIONALES.  No incluya las "<>" o"[]" cuando use los comandos.

Los siguientes comandos son válidos:  

    $commands

Las preguntas y otras cosas que requieran la atención de una persona deben enviarse a:

    $administrator ipython no está disponible, poner use_ipython en no Como administrador de la lista, se solicita su autorización para
la siguiente publicación en la lista de correo:

    List:    $listname
    From:    $sender_email 
   Subject: $subject

El mensaje se ha retenido porque:

$reasons

Cuando pueda, visite su panel de control para aprobar o
denegar la solicitud. Se requiere su autorización para aprobar una suscripción a una lista de correo:

  Para:  $member 
  Lista: $listname Se requiere su autorización para aprobar una solicitud de baja de una lista de correo:
	De:	$member
	Lista: $listname" La suscripción de $member a $listanme se ha desactivado porque su puntuación de rebotes
excede el límite bounce_score_threshold de la lista. Se ha cancelado la suscripción de $member a $display_name por tener demasiados rebotes. $member se ha suscrito correctamente a $display_name. El mensaje adjunto se recibió como un rebote, pero o bien no se reconoció
el formato de rebote, o bien no se pudo extraer de él ninguna suscrita.
Esta lista de correo se ha configurado para enviar todos los mensajes rebotados
no reconocidos a los administradores de la lista. $member ha sido eliminado de $display_name.   Los mensajes a la lista de correo $display_name deben enviarse a
	$listname

Para suscribirse o cancelar la suscripción por correo, envíe un mensaje con el asunto
o el cuerpo 'help' a
	$request_email

Puede contactar con la persona que gestiona la lista en
	$owner_email

Cuando responda, por favor, edite el Asunto del mensaje para que sea más específico que
"Re: Contenido de $display_name digest..." _______________________________________________
Lista de correo $display_name -- $listname
Para darse de baja, envíe un correo a ${short_listname}-leave@${domain}   El propietario de la lista de correo $short_listname en $domain le
ha invitado a que se suscriba a la misma con su dirección
"$user_email".
Puede aceptar la invitación simplemente respondiendo a este mensaje.

O puede enviar un mensaje a $request_email con la siguiente línea 
-- únicamente con dicha línea --:

    confirm $token

En la mayoría de los programas de correo suele bastar con responder
al presente mensaje.

Si quiere declinar la invitación, por favor, ignore este mensaje.
Si tiene alguna pregunta, por favor, póngase en contacto con
$owner_email. Confirmación de registro de la dirección de correo

Hola, este es el servidor de GNU Mailman en $domain.

Hemos recibido una solicitud de registro de la dirección de correo $user_email

Antes de que pueda empezar a usar GNU Mailman en este sitio, debe confirmar primero
que esta es su dirección de correo. Puede hacerlo respondiendo a este mensaje.

O puede enviar un mensaje a $request_email que incluya solo la siguiente linea -- 
es muy importante que incluya solo ésta línea:

    confirm $token

En la mayoría de programas de correo electrónico suele bastar con 'Responder'
a este mensaje.

Si no desea registrar esta dirección de correo, simplemente ignore este mensaje. 

Si cree que estás siendo suscrito maliciosamente a la lista, o tiene alguna otra pregunta,
puede contactar con

   $owner_email Confirmación de cancelación de la suscripción de la dirección de correo

Hola, este es el servidor de GNU Mailman en $domain.

Hemos recibido una solicitud de cancelación de la suscripción de la dirección de correo

    $user_email

Antes de que GNU Mailman pueda cancelar su suscripción, debe confirmar primero su solicitud.
Puede hacerlo respondiendo a este mensaje.

O puede enviar un mensaje a $request_email que incluya solo la siguiente linea -- 
es muy importante que incluya solo ésta línea:

    confirm $token

En la mayoría de programas de correo electrónico suele bastar con 'Responder'
a este mensaje.

Si no desea cancelar la suscripción de esta dirección de correo,
simplemente haga caso omiso a este mensaje.

Si cree que se le está dando de baja de la lista maliciosamente, o tiene alguna otra pregunta,
puede contactar con

   $owner_email   Su correo a '$listname' con el asunto

    $subject

Está retenido hasta que el moderador de la lista pueda revisarlo para su aprobación.

El mensaje está retenido porque:

$reasons

El mensaje se publicará en la lista, o, si no se publica,
 recibirá una notificación de la decisión del moderador. Hemos recibido un mensaje de su dirección <$sender_email> solicitando una
respuesta automática de la lista de correo $listname.

Hoy hemos visto $count de ellos.

Para evitar problemas como bucles de correo entre los robots de correo,
no le enviaremos más respuestas hoy.  Por favor, inténtelo de nuevo mañana

Si cree que este mensaje es un error, o si tiene alguna pregunta, por favor,
póngase en contacto con el propietario de la lista en $owner_email. Su mensaje titulado

    $subject

se recibió con éxito por la lista de correo $display_name . Este es un mensaje de detección.  Puede ignorarlo sin problemas.

La lista de correo $listname ha recibido varios rebotes suyos,lo que indica
que puede haber un problema en la entrega de mensajes a $sender_email.
Se adjunta un ejemplo a continuación. Por favor, examine este mensaje para
asegurarse de que no hay problemas con su dirección de correo.
Puede contactar con su administrador de correo para más ayuda.

No es necesario que haga nada para seguir siendo un miembro activo de la lista de correo.

Si tiene alguna pregunta o problema, puede ponerse en contacto con
el propietario de la lista de correo en la dirección $owner_email Su solicitud a la lista de correo $listname 
  $request

ha sido rechazada por el moderador de la lista. 

El moderador dio la siguiente razón para rechazar su solicitud:

"$reason"

Cualquier pregunta o comentario debe dirigirse al administrador de la lista en:

   $owner_email" Su mensaje a la lista de correo de $listname fue rechazado por
las siguientes razones:

$reasons

Se adjunta el mensaje original tal como fue recibido por Mailman. Se ha desactivado si suscripción a la lista de correo $listname porque se han
recibido varios rebotes, lo que indica que puede haber problemas de entrega
de mensajes a la dirección $sender_email. Puede contactar con su administrador
de correo electrónico para obtener más ayuda.

Si tiene preguntas o problemas, puede contactar con el administrador de la lista
de correo en la dirección:

    $owner_email ¡Bienvenido a la lista de correo de "$display_name"!

Para enviar a esta lista, envíe su mensaje a:

    $listname

Puede cancelar la suscripción o hacer ajustes a sus opciones por correo
enviando un mensaje a:

   $request_email

con la palabra 'help' en el asunto o en el cuerpo (no incluya las comillas),
y recibirá un mensaje con instrucciones.
Necesitará su contraseña para cambiar sus opciones, pero por motivos de seguridad,
esta contraseña no está incluida aquí.
Si ha olvidado su contraseña, tendrá que restablecerla a través de la interfaz de usuario de la web. n/d readline no disponible la sección y rango deben ser enteros: $value 